package be.multimedi.oplossingen.h04o3x;

import java.util.Scanner;

public class AgeApp {
    public static void main(String[] args) {
        var keyboard = new Scanner(System.in);

        System.out.print("Enter your age: ");
        var age = keyboard.nextInt();

        if (age >= 18) {
            System.out.println("You are an adult");
        } else {
            System.out.println("You are a child");
        }
    }
}