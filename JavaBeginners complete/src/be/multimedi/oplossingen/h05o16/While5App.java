package be.multimedi.oplossingen.h05o16;

import java.util.Scanner;

public class While5App {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a number between 0 and 10: ");
        int number = keyboard.nextInt();
        while(number < 0 || number > 9) {
            System.out.println("Wrong number!");
            System.out.print("Enter a number between 0 and 10: ");
            number = keyboard.nextInt();
        }
        System.out.println("Correct number: " + number);
    }
}
