package be.multimedi.oplossingen.h05o16;

public class While2App {
    public static void main(String[] args){
        int number = 0;
        while(number*3<50){
            System.out.println("3 * " + number + " = " + (number *3));
            number++;
        }
    }
}
