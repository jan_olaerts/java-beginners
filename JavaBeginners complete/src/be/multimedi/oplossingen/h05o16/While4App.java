package be.multimedi.oplossingen.h05o16;

public class While4App {
    public static void main(String[] args) {
        char ch = 'A';
        while(ch<='Z'){
            System.out.print(" " + ch++);
        }
    }
}
