package be.multimedi.oplossingen.h05o16;

public class While3App {
    public static void main(String[] args){
        int number = 1;
        int power = 0;
        while(number<10000){
            System.out.println( "5^" + power + " = " + number );

            // volgende macht berekenen
            power++;
            int count = 0;
            number = 1;
            while(count < power){
                count++;
                number *= 5;
            }
        }
    }
}
