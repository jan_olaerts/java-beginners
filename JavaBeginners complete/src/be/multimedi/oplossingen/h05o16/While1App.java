package be.multimedi.oplossingen.h05o16;

public class While1App {
    public static void main(String[] args) {
        int number = 120;
        while (number >= 100) {
            System.out.println(number);
            number--;
        }
    }
}
