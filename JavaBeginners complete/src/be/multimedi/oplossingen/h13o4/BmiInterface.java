package be.multimedi.oplossingen.h13o4;

public abstract interface BmiInterface {
   public abstract float calculateBmi(int weight, int height);

   public abstract void printDiagnose(float bmi);
}
