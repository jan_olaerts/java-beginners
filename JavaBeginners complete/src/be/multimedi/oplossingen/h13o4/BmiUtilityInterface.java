package be.multimedi.oplossingen.h13o4;

public abstract interface BmiUtilityInterface {
   public abstract float calculateBmi(int weight, int height);

   public abstract void printDiagnose(float bmi);
}
