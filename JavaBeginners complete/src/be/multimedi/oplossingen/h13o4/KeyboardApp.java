package be.multimedi.oplossingen.h13o4;

public class KeyboardApp {
   public static void main(String[] args) {
      KeyboardUtilityInterface ki = new KeyboardUtility();
      System.out.print("Enter your length (cm): ");
      int length = ki.readInt();

      System.out.print("Enter your weight (kg): ");
      int weight = ki.readInt();

      BmiUtilityInterface bi = new BmiUtility();
      float bmi = bi.calculateBmi(weight, length);
      bi.printDiagnose(bmi);
   }
}
