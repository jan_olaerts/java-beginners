package be.multimedi.oplossingen.h13o4;

import java.util.Scanner;

public class KeyboardUtility implements KeyboardUtilityInterface {
   public int readInt() {
      Scanner keyboard = new Scanner(System.in);
      return keyboard.nextInt();
   }
}
