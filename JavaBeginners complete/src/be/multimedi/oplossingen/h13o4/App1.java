package be.multimedi.oplossingen.h13o4;

public class App1 {
   public static void main(String[] args) {
      KeyboardUtilityInterface keyboardUtility = new KeyboardUtility();
      System.out.print("Enter your length (cm): ");
      int length = keyboardUtility.readInt();

      BmiUtilityInterface bmiUtility = new BmiUtility();
      System.out.print("Enter your weight (kg): ");
      int weight = keyboardUtility.readInt();
      float bmi = bmiUtility.calculateBmi(weight, length);
      bmiUtility.printDiagnose(bmi);
   }
}
