package be.multimedi.oplossingen.h05o07;

public class Main {
    public static void main(String[] args){
        boolean value1 = true;
        boolean value2 = false;
        System.out.println("value1\t: " + value1);
        System.out.println("value2\t: " + value2);
        System.out.println("value1 && value2\t: " + (value1 && value2));
        System.out.println("value1 || value2\t: " + (value1 || value2));
        System.out.println("!value1\t: " + !value1);
        System.out.println("!value2\t: " + !value2);
    }
}
