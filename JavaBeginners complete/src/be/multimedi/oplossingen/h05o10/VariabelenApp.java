package be.multimedi.oplossingen.h05o10;

public class VariabelenApp {
    public static void main(String[] args) {
        int number1 = 57;
        int number2 = 33;
        System.out.println("number1\t: " + number1);
        System.out.println("number2\t: " + number2);
        number1 += number2;
        System.out.println("number1 += number2; -> number1: " + number1);
        number1 -= number2;
        System.out.println("number1 -= number2; -> number1: " + number1);
        number1 *= number2;
        System.out.println("number1 *= number2; -> number1: " + number1);
        number1 /= number2;
        System.out.println("number1 /= number2; -> number1: " + number1);
        number1 %= number2;
        System.out.println("number1 %= number2; -> number1: " + number1);
        number1 &= number2;
        System.out.println("number1 &= number2; -> number1: " + number1);
        number1 |= number2;
        System.out.println("number1 |= number2; -> number1: " + number1);
        number1 ^= number2;
        System.out.println("number1 ^= number2; -> number1: " + number1);
        number1 >>= number2;
        System.out.println("number1 >>= number2; -> number1: " + number1);
        number1 <<= number2;
        System.out.println("number1 <<= number2; -> number1: " + number1);
        number1 >>>= number2;
        System.out.println("number1 >>>= number2; -> number1: " + number1);
    }
}
