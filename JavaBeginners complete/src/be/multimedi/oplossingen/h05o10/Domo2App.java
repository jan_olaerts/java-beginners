package be.multimedi.oplossingen.h05o10;

public class Domo2App {
    public static String intToBinary(int number){
        StringBuilder sb = new StringBuilder(String.format("%8s", Integer.toBinaryString(number)).replaceAll(" ", "0"));
        sb.insert(4, ' ');
        return sb.toString();
    }

    public static void main(String[] args){
        final int HALL       = 0x01;
        final int LIVINGROOM = 0x02;
        final int KITCHEN    = 0x04;
        final int VERANDA    = 0x08;
        final int BEDROOM    = 0x10;
        final int LAUNDRY    = 0x20;
        final int BATHROOM   = 0x40;
        final int CELLAR     = 0x80;

        final int DOWNSTAIRS = HALL | KITCHEN | LIVINGROOM | VERANDA;
        final int UPSTAIRS = BEDROOM | LAUNDRY | BATHROOM;

        int heating = 0;
        System.out.println("Starting condition heating\t: " + intToBinary(heating));

        heating |= LIVINGROOM;
        System.out.println("Heating in living room on\t: " + intToBinary(heating));

        heating &= ~LIVINGROOM;
        System.out.println("Heating in living room off\t: " + intToBinary(heating));

        heating |= DOWNSTAIRS;
        System.out.println("Heating downstairs on\t\t: " + intToBinary(heating));

        heating |= UPSTAIRS;
        System.out.println("Heating upstairs on\t\t\t: " + intToBinary(heating));

        heating ^= DOWNSTAIRS;
        System.out.println("Heating downstairs switched\t: " + intToBinary(heating));

        System.out.println("Check heating in bathroom\t: " + ((heating & BATHROOM) == BATHROOM));

        heating &= ~UPSTAIRS;
        System.out.println("Heating upstairs off\t\t: " + intToBinary(heating));

        heating |= CELLAR;
        System.out.println("Heating cellar on\t\t\t: " + intToBinary(heating));

        heating = 0;
        System.out.println("All off\t\t\t\t\t\t: " + intToBinary(heating));
    }
}
