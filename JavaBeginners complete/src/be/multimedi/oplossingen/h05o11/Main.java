package be.multimedi.oplossingen.h05o11;

public class Main {
    public static void main(String[] args){
        int number1 = 57;
        int number2 = 33;
        int number3 = -65;
        System.out.println("number1\t: " + number1);
        System.out.println("number2\t: " + number2);
        System.out.println("number3\t: " + number3);
        System.out.println("Smallest number");

        int temp = number1<number2?number1:number2;
        System.out.println("number1 or number2\t: " + temp);

        temp = number1<number3?number1:number3;
        System.out.println("number1 or number3\t: " + temp);

        temp = number2<number1?number2:number1;
        System.out.println("number2 or number1\t: " + temp);

        temp = number2<number3?number2:number3;
        System.out.println("number2 or number3\t: " + temp);

        temp = number3<number1?number3:number1;
        System.out.println("number3 or number1\t: " + temp);

        temp = number3<number2?number3:number2;
        System.out.println("number3 or number2\t: " + temp);
    }
}
