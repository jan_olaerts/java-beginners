package be.multimedi.oplossingen.h08o06;

public class RectangleApp {
    public static void main(String[] args){
        System.out.println("This program uses a rectangle");
        Rectangle rect = new Rectangle();
        rect.setX(-10);
        rect.setY(-11);
        rect.setWidth(-12);
        rect.setHeight(-13);

        System.out.println("X\t: " + rect.getX());
        System.out.println("Y\t: " + rect.getY());
        System.out.println("Width\t: " + rect.getWidth());
        System.out.println("Height\t: " + rect.getHeight());

        System.out.println("Area\t: " + rect.getArea());
        System.out.println("Perimeter\t: " + rect.getPerimeter());

        System.out.println("*** grow -100 ***");
        rect.grow(-100);

        System.out.println("X\t: " + rect.getX());
        System.out.println("Y\t: " + rect.getY());
        System.out.println("Width\t: " + rect.getWidth());
        System.out.println("Height\t: " + rect.getHeight());

        System.out.println("Area\t: " + rect.getArea());
        System.out.println("Perimeter\t: " + rect.getPerimeter());
    }
}
