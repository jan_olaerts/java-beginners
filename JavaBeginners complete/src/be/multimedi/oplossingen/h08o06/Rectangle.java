package be.multimedi.oplossingen.h08o06;

public class Rectangle {
   private int x;
   private int y;
   private int height;
   private int width;

   public int getX() {
      return x;
   }

   public void setX(int x) {
      this.x = x;
   }

   public int getY() {
      return y;
   }

   public void setY(int y) {
      this.y = y;
   }

   public int getHeight() {
      return height;
   }

   public void setHeight(int height) {
      this.height = height < 0 ? -height : height;
   }

   public int getWidth() {
      return width;
   }

   public void setWidth(int width) {
      this.width = width < 0 ? -width : width;
   }

   public void setPosition(int x, int y) {
      setX(x);
      setY(y);
   }

   public void grow(int d) {
      setWidth(width + d);
      setHeight(height + d);
   }

   public double getArea() {
      return (double) height * width;
   }

   public double getPerimeter() {
      return (double) width * 2 + height * 2;
   }
}
