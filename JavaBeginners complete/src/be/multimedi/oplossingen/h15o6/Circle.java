package be.multimedi.oplossingen.h15o6;

import java.util.Formatter;

public class Circle extends Shape {
    private static int count = 0;
    private int radius;

    public Circle() {
        this(0);
    }

    public Circle(int radius) {
        this(radius, 0, 0);
    }

    public Circle(int radius, int x, int y) {
        count++;
        setRadius(radius);
        setPosition(x, y);
    }

    public Circle(Circle c) {
        this(c.radius, c.x, c.y);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        if (radius < 0) throw new NegativeSizeException("Negative radius");
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    @Override
    public void grow(int d) {
        setRadius(radius + d);
    }

    @Override
    public String toString() {
        Formatter f = new Formatter().format("Circle:");
        f.format("\n\tRadius: %d", radius);
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    @Override
    public int hashCode() {
        return getX() * 7 + getY() * 13 + getRadius() * 17;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (getClass() == o.getClass()) {
                Circle c = (Circle) o;
                if ((c.getX() == getX()) &&
                        (c.getY() == getY()) &&
                        c.getRadius() == getRadius()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void scale(int factor) {
        setRadius((int) ((double) factor / 100 * radius));
    }

    public static int getCount() {
        return count;
    }
}
