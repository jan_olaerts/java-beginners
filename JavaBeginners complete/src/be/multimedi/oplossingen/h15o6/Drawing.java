package be.multimedi.oplossingen.h15o6;

import java.util.Formatter;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Drawing implements Drawable, Iterable<Drawable> {
    static final int MAX_SIZE = 16;
    private int size;
    private Drawable[] list = new Drawable[MAX_SIZE];

    public void remove(Drawable d) {
        // look for d
        for (int i = 0; i < size; i++) {
            // is this the one?
            if (d == list[i]) {
                // move all the others 1 step forward
                for (int j = i + 1; j < size && j < list.length; j++) {
                    list[j - 1] = list[j];
                }
                // remove the last (double)reference an decrease size
                list[size--] = null;
                return;
            }
        }
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            list[i] = null;
        }
        size = 0;
    }

    public void add(Drawable d) {
        if (size >= MAX_SIZE) {
            return;
        }
        list[size] = d;
        size++;
    }

    public int getSize() {
        return size;
    }

    @Override
    public void draw(DrawingContext dc) {
        System.out.println("Drawing: ");
        for (Drawable d : list){
            if(d!=null){
                d.draw(dc);
                //s.draw();
            }
        }
    }

    @Override
    public String toString() {
        Formatter f = new Formatter();
        f.format("Drawing: %d/%d", size, MAX_SIZE);

        for(int i = 0; i<size; i++){
            Drawable d = list[i];
            if (d == null) break;
            f.format("\n %d>%s", i+1, d.toString());
        }
        return f.toString();
    }

    @Override
    public void scale(int factor) {
        for(int i = 0; i<size; i++){
            list[i].scale(factor);
        }
    }

    @Override
    public Iterator<Drawable> iterator() {
        return new DrawableIterator(this);
    }
    public class DrawableIterator implements Iterator<Drawable> {
        Drawing d;
        int index = 0;

        public DrawableIterator(Drawing d) {
            this.d = d;
        }

        @Override
        public boolean hasNext() {
            return index < d.size;
        }

        @Override
        public Drawable next() {
            if(index>=d.size) throw new NoSuchElementException();
            return d.list[index++];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
