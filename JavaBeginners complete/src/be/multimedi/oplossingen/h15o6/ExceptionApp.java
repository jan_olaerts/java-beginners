package be.multimedi.oplossingen.h15o6;

import java.util.Scanner;

public class ExceptionApp {
    public static void main(String[] args){
        try{
            int number = readInt();
            System.out.println("Number: " + number);
        } catch (NegativeSizeException e)
        {
            System.out.println("Exception: " + e.getMessage());
            //e.printStackTrace();
        }
    }

    public static int readInt() throws NegativeSizeException{
        Scanner keyb = new Scanner(System.in);
        System.out.print( "Enter a number: ");
        int number = keyb.nextInt();
        if( number<0 ){
            throw new NegativeSizeException("Negative input value: " + number);
        }
        return number;
    }
}
