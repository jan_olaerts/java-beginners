package be.multimedi.oplossingen.h15o6;

public class MainApp {
   // Met throw NegativeSizeException
   public static void main(String[] args) {
      test();
      System.out.println("Hello World");
   }

   public static void test() {
      var rect = new Rectangle();
      rect.setHeight(-1);
      System.out.println("Achter Exception");
      System.out.println("The end");
   }
}
