package be.multimedi.oplossingen.h15o6;

public interface Drawable extends Scaleable {
    public void draw(DrawingContext dc);
}
