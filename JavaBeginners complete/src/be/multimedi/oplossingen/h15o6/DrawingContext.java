package be.multimedi.oplossingen.h15o6;

public interface DrawingContext {
    public void draw(Rectangle rect);
    public void draw(Circle circle);
    public void draw(Triangle triangle);
}
