package be.multimedi.oplossingen.h05o18;

public class BmiUtility {
    public static float calculateBmi(int weight, int lenght){
        float len = lenght / 100F;
        return (float)weight /(len*len);
    }

    public static void printDiagnose(float bmi){
        System.out.println("Your BMI: " + bmi);
        if (bmi < 20) {
            System.out.println("  underweight");
        } else if(bmi < 25){
            System.out.println("  ok");
        } else if(bmi < 30){
            System.out.println("  overweight");
        } else if(bmi < 40){
            System.out.println("  obesitas");
        } else {
            System.out.println("  extreme overweight");
        }
    }
}
