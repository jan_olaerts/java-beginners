package be.multimedi.oplossingen.h05o18;

public class KeyboardApp {
    public static void main(String[] args){
       System.out.print("Enter your length (cm): ");
       int lenght = KeyboardUtility.readInt();
       System.out.print("Enter your weight (kg): ");
       int weight = KeyboardUtility.readInt();
       float bmi = BmiUtility.calculateBmi(weight, lenght);
       BmiUtility.printDiagnose(bmi);
    }
}
