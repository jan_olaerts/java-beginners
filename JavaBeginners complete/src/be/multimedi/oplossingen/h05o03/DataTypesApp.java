package be.multimedi.oplossingen.h05o03;

public class DataTypesApp {
    public static void main(String[] args) {
        boolean aBoolean = false;
        char aCharacter = 'd';
        byte aByte = 126;
        short aShortInteger = 115;//1568;
        int anInteger = 1256456;
        long aLongInteger = 45631341L;
        float aDecimalNumber = 1256.32F;
        double aBigDecimalNumber = 12.365987451236;

        System.out.println(aBoolean);
        System.out.println(aCharacter);
        System.out.println(aByte);
        System.out.println(aShortInteger);
        System.out.println(anInteger);
        System.out.println(aLongInteger);
        System.out.println(aDecimalNumber);
        System.out.println(aBigDecimalNumber);
        System.out.println("***");

        anInteger = 0342; //octale waarde
        System.out.println(anInteger);
        anInteger = 0x56_31; //hexadecimale waarde
        System.out.println(anInteger);
        anInteger = 0b0101_1100; //binaire waarde
        System.out.println(anInteger);
        System.out.println("***");

        final double PI = 3.14;
        //PI = 3.15;//   Error: Java: cannot assign a value to final variable PI
        System.out.println(PI);
        System.out.println("***");

        //aByte = aShortInteger;//    Error: Java: incompatiblee types: possible lossy conversion from short to byte
        aByte = (byte) aShortInteger;
        System.out.println(aByte);

        //aShortInteger = aCharacter;//   Error: Java: incompatible types: possible lossy conversion from char to short
        aShortInteger = (short)aCharacter;
        System.out.println(aShortInteger);

        anInteger = aCharacter;
        System.out.println(anInteger);
    }
}
