package be.multimedi.oplossingen.h08o03;

public class RectangleApp {
    public static void main(String[] args){
        System.out.println("This program uses a rectangle");
        Rectangle rect = new Rectangle();
        rect.x = 10;
        rect.y = 11;
        rect.width = 12;
        rect.height = 13;

        System.out.println("X\t: " + rect.x);
        System.out.println("Y\t: " + rect.y);
        System.out.println("Width\t: " + rect.width);
        System.out.println("Height\t: " + rect.height);

        Rectangle rect2 = new Rectangle();
        rect.x = 20;
        rect.y = 21;
        rect.width = 22;
        rect.height = 23;

        System.out.println("X\t: " + rect.x);
        System.out.println("Y\t: " + rect.y);
        System.out.println("Width\t: " + rect.width);
        System.out.println("Height\t: " + rect.height);
    }
}
