package be.multimedi.oplossingen.h06o2;

import java.net.URI;

/**
 * This program will open the default browser
 * and open the local Java-api-docs
 * on the page about the class String
 */
public class StringApiApp {
    public static void main(String[] args){
        String home = "C:/Users/Ward/Documents/";
        String url = home + "Java-docs/api/java.base/java/lang/String.html";
        try {
            java.awt.Desktop.getDesktop().browse(URI.create(url));
        }catch (Exception e){
            System.out.println( "Error: " +  e.getMessage());
            e.printStackTrace();
        }
    }
}
