package be.multimedi.oplossingen.h14o1;

public class Musician {
    public class Instrument{
        public void makeSound(){
            System.out.println("drums rumbling");
        }
    }

    public void play(){
        Instrument i = new Instrument();
        i.makeSound();
    }
}