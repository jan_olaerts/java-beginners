package be.multimedi.oplossingen.h14o1;

public class Main {
    public static void main(String[] args){
        Musician m = new Musician();
        m.play();

        Musician.Instrument i = m.new Instrument();
        i.makeSound();
    }
}
