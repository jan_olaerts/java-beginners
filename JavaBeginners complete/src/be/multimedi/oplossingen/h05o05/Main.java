package be.multimedi.oplossingen.h05o05;

public class Main {
    public static void main(String[] args){
        byte b1 = 4;
        byte b2 = 5;
        //byte b3 = b1 + b2; //Error: Incompatible types: possible lossy conversion from int to byte
        byte b3 = (byte)(b1 + b2);
        System.out.println("b3\t: "+ b3);
        int i1 = 2147483645;
        int i2 = 2147483642;
        int i3 = i1 + i2;
        System.out.println("i3\t: "+ i3); // i3 = -9 (overflow)
        long l = (long)i1 + i2;
        System.out.println("l\t: "+ l); // 4294967287
    }
}
