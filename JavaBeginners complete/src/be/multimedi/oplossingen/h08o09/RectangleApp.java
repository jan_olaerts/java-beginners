package be.multimedi.oplossingen.h08o09;

public class RectangleApp {
    public static void main(String[] args){
        System.out.println("This program uses a rectangle");
        Rectangle rect = new Rectangle(10,11,12,13);

        System.out.println("X\t: " + rect.getX());
        System.out.println("Y\t: " + rect.getY());
        System.out.println("Width\t: " + rect.getWidth());
        System.out.println("Height\t: " + rect.getHeight());

        System.out.println("Area\t: " + rect.getArea());
        System.out.println("Perimeter\t: " + rect.getPerimeter());

        System.out.println("Count\t: " + Rectangle.count);
        System.out.println("Angles\t: " + Rectangle.ANGLES);

        System.out.println("Creating 3 Objects of class Rectangle");
        Rectangle rect2 = new Rectangle();
        Rectangle rect3 = new Rectangle();
        Rectangle rect4 = new Rectangle();
        System.out.println("Count\t: " + Rectangle.count);
    }
}
