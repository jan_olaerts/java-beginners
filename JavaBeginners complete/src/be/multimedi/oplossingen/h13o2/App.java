package be.multimedi.oplossingen.h13o2;

public class App {
   public static void main(String[] args) {
      Drawing drawing = new Drawing();
      Rectangle rectangle = new Rectangle(10, 10);
      Square square = new Square(40);
      Triangle triangle = new Triangle(10, 20, 30);
      Circle circle = new Circle(20);

      drawing.add(rectangle);
      drawing.add(triangle);
      drawing.add(square);
      drawing.add(circle);

      System.out.println(drawing.toString());
      System.out.println("*** draw ***");
      drawing.draw(new TextDrawingContext());
   }
}