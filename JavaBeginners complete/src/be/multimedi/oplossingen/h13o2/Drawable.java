package be.multimedi.oplossingen.h13o2;

public interface Drawable extends Scaleable {
   public void draw(DrawingContext dc);
}
