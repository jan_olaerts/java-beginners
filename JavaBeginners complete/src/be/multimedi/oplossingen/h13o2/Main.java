package be.multimedi.oplossingen.h13o2;

import java.util.Formatter;

public class Main {
   public static void main(String[] args) {
      Drawing d = new Drawing();
      Rectangle rect = new Rectangle(10, 10);
      Square squa = new Square(40);
      Triangle tria = new Triangle(10, 20, 30);
      Circle circ = new Circle(20);

      d.add(rect);
      d.add(tria);
      d.add(squa);
      d.add(circ);
      d.scaleDouble();
      System.out.println(d.toString());

      tria.scaleDouble();
      circ.scaleHalf();
      d.remove(rect);
      d.remove(squa);
      System.out.println(d.toString());

      d.clear();
      System.out.println(d.toString());

      printShapeCount();
   }

   public static void printShapeCount() {
      Formatter f = new Formatter();
      f.format("Shapes: %d", Shape.getCount());
      f.format("\n Rectangles: %d", Rectangle.getCount());
      f.format("\n Squares: %d", Square.getCount());
      f.format("\n Circles: %d", Circle.getCount());
      f.format("\n Triangles: %d", Triangle.getCount());
      System.out.println(f.toString());
   }
}
