package be.multimedi.oplossingen.h13o2;

public class Drawing implements Drawable {
   static final int LIST_MAX_SIZE = 4;
   private int listSizeMultiplyer = 1;
   private Drawable[] drawables = new Drawable[LIST_MAX_SIZE];
   private int size = 0;

   public void add(Drawable drawable) {
      if (drawable == null || size >= drawables.length || locate(drawable) != -1) {
         if (size >= drawables.length) {
            System.out.println("List full, resizing");
            Drawable[] tempDrawables = new Drawable[LIST_MAX_SIZE * (++listSizeMultiplyer)];
            int i = 0;
            for (Drawable d : drawables) {
               tempDrawables[i++] = d;
            }
            drawables = tempDrawables;
            System.out.println("List size: " + drawables.length);
         } else {
            System.out.println("Not adding shape: " + drawable);
            if (drawable != null) {
               System.out.println("The shape is already in the list");
            }
            return;
         }
      }
      drawables[getFreeLocation()] = drawable;
      size++;
   }

   public boolean isPersent(Shape shape) {
      return shape != null && locate(shape) != -1;
   }

   private int locate(Drawable drawable) {
      for (int i = 0; i < drawables.length; i++) {
         if (drawables[i] == drawable) {
            return i;
         }
      }
      return -1;
   }

   public void remove(Drawable drawable) {
      int l = locate(drawable);
      if (l > 0) {
         if (l == size - 1)
            drawables[l] = null;
         else {
            drawables[l] = drawables[size - 1];
            drawables[size - 1] = null;
         }
         size--;
      } else {
         System.out.println("shape was never in drawing to begin with: " + drawable);
      }
   }

   private int getFreeLocation() {
      return locate(null);
   }

   public void clear() {
      //for (Drawable d : drawables) d = null;
      for (int i = 0; i < size; i++) {
         drawables[i] = null;
      }
      size = 0;
   }

   public int getSize() {
      return size;
   }

   @Override
   public void draw(DrawingContext dc) {
      System.out.println("Drawing: ");
      for (Drawable d : drawables) {
         if (d != null) {
            d.draw(dc);
            //s.draw();
         }
      }
   }

   @Override
   public void scale(int factor) {
      for (int i = 0; i < size; i++) {
         drawables[i].scale(factor);
      }
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder("Drawing:\n");
      sb.append("  Shapes: " + size + " {\n");
      for (Drawable d : drawables) {
         if (d == null) continue;
         sb.append('\t');
         sb.append(d.toString().replaceAll("\n", "\n\t"));
         sb.append('\n');
      }
      sb.append("  }\n");
      return sb.toString();
   }
}

