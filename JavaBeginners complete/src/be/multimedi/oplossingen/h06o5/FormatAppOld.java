package be.multimedi.oplossingen.h06o5;

import java.util.Scanner;

public class FormatAppOld {
   public static void main(String[] args) {
      var keyboard = new Scanner(System.in);
      int getal1,getal2,som=0,teller=0,quotient;

      System.out.println("Geef eerste getal: ");
      getal1 = keyboard.nextInt();
      System.out.println("Geef tweede getal: ");
      getal2 = keyboard.nextInt();
      quotient=1;

      for (int i = getal1+1; i < getal2 ; i++) {
         som += i;
         quotient *= i;
      }
      System.out.print("De som van alle tussenliggende getallen = ");
      System.out.println(som);
      System.out.print("De vermenigvuldiging van alle tussenliggende getallen = ");
      System.out.println(quotient);

   }
}

