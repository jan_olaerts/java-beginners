package be.multimedi.oplossingen.h06o5;

import java.util.Formatter;

public class FormatApp {
    public static void main(String[] args) {
        for(int i = 2; i <= 40; i++){
            float meter = (float)i/2;
//            Formatter f = new Formatter();
//            f.format("%5.2f meter = %5.2f feet\n", meter, meterToFeet(meter) );
//            System.out.println(f.toString());
            System.out.printf("%5.2f meter = %5.2f feet\n", meter, meterToFeet(meter) );
        }
    }
    public static float meterToFeet(float meter){
        return meter * 3.2808399F;
    }
}
