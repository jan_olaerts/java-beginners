package be.multimedi.oplossingen.h12o3;

import java.time.DayOfWeek;
import java.util.Scanner;

public class MonthDayApp {
    public static void main(String[] args) {
        Scanner keyb = new Scanner(System.in);
        System.out.print("Enter a weekday (1-7): ");
        int number = keyb.nextInt();
        System.out.print("Enter a number of days to skip: ");
        int skip = keyb.nextInt();
        number = (number-1+skip)%7+1;
        DayOfWeek d = DayOfWeek.of(number);
        System.out.println("Day : " + d);
    }
}
