package be.multimedi.oplossingen.h10o03;

public class Square extends Rectangle {
    public static String DESCRIPTION = "Square";

    public void setSide( int side ){
        setWidth(side);
        setHeight(side);
    }

    public int getSide(){
        return getHeight();
    }
}
