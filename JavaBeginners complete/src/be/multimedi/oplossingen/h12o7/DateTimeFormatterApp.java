package be.multimedi.oplossingen.h12o7;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateTimeFormatterApp {
    public static void main(String[] args){
        final String FORMAT1 = "dd/MM/uuuu";
        final String FORMAT2 = "uuuu-MM-dd";
        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern(FORMAT1);
        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern(FORMAT2);

        Scanner keyb = new Scanner(System.in);
        System.out.print("Enter a date (" + FORMAT1 + "): ");
        String text = keyb.nextLine();
        LocalDate ld = LocalDate.parse(text, dtf1);
        System.out.println("Date (" + FORMAT1 + "): " + ld.format(dtf1));
        System.out.println("Date (" + FORMAT2 + "): " + ld.format(dtf2));
        System.out.printf("Date direct format: %1$td.%1$tm.%1$tY", ld);
    }
}
