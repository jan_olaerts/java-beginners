package be.multimedi.oplossingen.h13o1;

public interface Scalable {
   public static final int DOUBLE = 200;
   public static final int HALF = 50;
   public static final int QUARTER = 25;

   public abstract void scale(int factor);
}
