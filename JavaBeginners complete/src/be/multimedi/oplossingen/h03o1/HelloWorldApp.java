/**
 * This Java application shows
 * the text 'Hello World!' on the screen
 */
package be.multimedi.oplossingen.h03o1;

/* Using the terminal to run the code in Java 1.11:
 * =================================================
 *  running source code:
 *      java src/be/multimedi/oplossingen/h03o1/HelloWorldApp.java
 *  Compiling:
 *      javac -d out\production\JavaGevorderden src\be\multimedi\oplossingen\h22\*
 *  Creating Jar:
 *      jar cfe HelloWorld.jar be.multimedi.oplossingen.h22.HelloWorld -C out\production\JavaGevorderden .
 *  Running Jar:
 *      java -jar HelloWorld.jar
 */
/* Checking JAVA_HOME
 *  ------------------
 *  echo %JAVA_HOME%:
 *      C:\Program Files\Java\jdk-11.0.2
 *  javac -version:
 *      javac 11.0.2
 *  java -version:
 *      java version "11.0.2" 2019-01-15 LTS
 *      Java(TM) SE Runtime Environment 18.9 (build 11.0.2+9-LTS)
 *      Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.2+9-LTS, mixed mode)
 */
/* Shortcut:
 *  ctrl + shift + F10          : to run the active (this)file's main function
 */
public class HelloWorldApp {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}