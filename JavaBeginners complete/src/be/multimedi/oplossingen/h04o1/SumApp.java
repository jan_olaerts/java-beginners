package be.multimedi.oplossingen.h04o1;

import java.util.Scanner;

public class SumApp {
    public static void main(String[] args) {
        var keyboard = new Scanner(System.in);

        System.out.print("Enter a number: ");
        var a = keyboard.nextInt(); // Input

        System.out.print("Enter another number: ");
        var b = keyboard.nextInt(); // Input

        var sum = a + b;
        System.out.println("The sum is " + sum); // Output
    }
}