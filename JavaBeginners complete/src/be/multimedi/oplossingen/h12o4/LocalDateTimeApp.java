package be.multimedi.oplossingen.h12o4;


import java.time.LocalDate;

public class LocalDateTimeApp {
    public static void main(String[] args) {
        LocalDate l = LocalDate.of(1984, 9, 3);
        System.out.println("Day date: " + l);
        System.out.println("Day of year: " + l.getDayOfYear());
        System.out.println("Day of month: " + l.getDayOfMonth());
        System.out.println("Day of week: " + l.getDayOfWeek());
        System.out.println("Is leapyear: " + l.isLeapYear());
        System.out.println("Zelf Datum voorgesteld: " + l.getYear() + " " + l.getMonth() + " " + l.getDayOfMonth());
    }
}
