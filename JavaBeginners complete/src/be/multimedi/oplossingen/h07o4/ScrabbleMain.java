package be.multimedi.oplossingen.h07o4;

import java.util.Scanner;

public class ScrabbleMain {
    final static short[] scrablleValues = {1, 3, 3, 1, 1, 5, 2, 2, 1, 4, 4, 2, 3, 1, 1, 3, 10, 1, 1, 1, 4, 4, 4, 8, 8, 6};
    final static char BASE = 'a';

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a word: ");
        String word = keyboard.next();
        int total = 0;
        for (char c : word.toCharArray()) {
            int value = getScrabbleValue(c);
            System.out.println(c + " = " + value);
            total += value;
        }
        System.out.println(total);
    }

    public static int getScrabbleValue(char c) {
        int index = (int) c - (int) BASE;
        if (index < 0 || index >= scrablleValues.length)
            return 0;
        return scrablleValues[index];
    }
}
