package be.multimedi.oplossingen.h12o5;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TimeZoneApp {
    public static void main(String[] args){
        ZoneId londonZone = ZoneId.of("Europe/London");
        ZoneId sydneyZone = ZoneId.of("Australia/Sydney");
        ZoneId adelaideZone = ZoneId.of("Australia/Adelaide");
        ZoneId UTCZone = ZoneId.of("UTC-4");

        ZonedDateTime nowHere = ZonedDateTime.now();
        ZonedDateTime nowLondon = ZonedDateTime.now(londonZone);
        ZonedDateTime nowSydney = ZonedDateTime.now(sydneyZone);
        ZonedDateTime nowAdelaide = ZonedDateTime.now(adelaideZone);
        ZonedDateTime nowUTC = ZonedDateTime.now(UTCZone);

        System.out.println("Here: " + nowHere);
        System.out.println("London: " + nowLondon);
        System.out.println("Sydney: " + nowSydney);
        System.out.println("Adelaide: " + nowAdelaide);
        System.out.println("UTC-4: " + nowUTC);

        System.out.println();
        //shorter
        System.out.println("Here: " + ZonedDateTime.now());
        System.out.println("London: " + ZonedDateTime.now(ZoneId.of("Europe/London")));
        System.out.println("Sydney: " + ZonedDateTime.now(ZoneId.of("Australia/Sydney")));
        System.out.println("Adelaide: " + ZonedDateTime.now(ZoneId.of("Australia/Adelaide")));
        System.out.println("UTC-4: " + ZonedDateTime.now(ZoneId.of("UTC-4")));

        System.out.println();
        //formatted
        String pattern = "dd/MM/uuuu - HH:mm:ss";
        System.out.println("Here: " + ZonedDateTime.now().format(DateTimeFormatter.ofPattern(pattern)));
        System.out.println("London: " + ZonedDateTime.now(ZoneId.of("Europe/London")).format(DateTimeFormatter.ofPattern(pattern)));
        System.out.println("Sydney: " + ZonedDateTime.now(ZoneId.of("Australia/Sydney")).format(DateTimeFormatter.ofPattern(pattern)));
        System.out.println("Adelaide: " + ZonedDateTime.now(ZoneId.of("Australia/Adelaide")).format(DateTimeFormatter.ofPattern(pattern)));
        System.out.println("UTC-4: " + ZonedDateTime.now(ZoneId.of("UTC-4")).format(DateTimeFormatter.ofPattern(pattern)));    }
}
