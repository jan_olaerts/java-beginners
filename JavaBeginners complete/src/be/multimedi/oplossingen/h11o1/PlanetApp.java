package be.multimedi.oplossingen.h11o1;

public class PlanetApp {
   public static void main(String[] args) {
      for(Planet p : Planet.values()){
         System.out.println(String.format("Planet %8s: {distance %5.2f AU, mass %.2e Kg)", p.name(), p.getDistance(), p.getMass()));
      }
   }
}
