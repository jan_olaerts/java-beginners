package be.multimedi.oplossingen.h11o1;

public class CoinApp1 {
    public static void main(String[] args){
        float total = 0;
        for( Coin c : Coin.values()){
            total += c.getValue();
        }
        System.out.println("Sum of coins: " + total);
    }
}
