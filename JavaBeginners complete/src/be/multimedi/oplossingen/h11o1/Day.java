package be.multimedi.oplossingen.h11o1;

public enum Day {
    Monday(true),
    Tuesday(true),
    Wednesday(true),
    Thursday(true),
    Friday(true),
    Saturday(false),
    Sunday(false);

    boolean workday;
    Day(boolean workday){
        this.workday = workday;
    }

    public String toString(){
        return name() + "(" + ordinal() + ", " + workday + ")";
    }
}
