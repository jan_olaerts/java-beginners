package be.multimedi.oplossingen.h11o1;

public enum Coin {
    ONE_CENT(0.01F),
    TWO_CENT(0.02F),
    FIVE_CENT(0.05F),
    TEN_CENT(0.10F),
    TWENTY_CENT(0.20F),
    FIFTY_CENT(0.50F),
    ONE_EURO(1),
    TWO_EURO(2);

    private float value;
    Coin(float value){
        this.value = value;
    }

    public float getValue(){
        return value;
    }

    public String toString(){
        return name() + "(" + value + ")";
    }
}
