package be.multimedi.oplossingen.h11o1;

public class DayApp {
    public static void main(String[] args){
        System.out.println(Day.Monday);
        System.out.println(Day.Tuesday);
        System.out.println(Day.Wednesday);
        System.out.println(Day.Thursday);
        System.out.println(Day.Friday);
        System.out.println(Day.Saturday);
        System.out.println(Day.Sunday);

        System.out.println("\nOR\n"); //of
        for(Day d: Day.values()){
            System.out.println(d);
        }
    }
}
