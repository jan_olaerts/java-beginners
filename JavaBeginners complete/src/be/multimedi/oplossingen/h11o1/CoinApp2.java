package be.multimedi.oplossingen.h11o1;

public class CoinApp2 {
   public static void main(String[] args){
      int[] walletCoins = {2, 3, 6, 8, 10, 12, 1, 4, 0, 0 ,1};
      float total = 0;

      Coin[] coinList = Coin.values();
      for(int i = 0; i < coinList.length && i < walletCoins.length; i++){
         float temp = walletCoins[i] * coinList[i].getValue();
         System.out.println( String.format("%3d coins of %4.2f€ = %4.2f€",
                 walletCoins[i], coinList[i].getValue(), temp));
         total += temp;
      }
      System.out.println("Sum of coins: " + total + "€");
   }
}
