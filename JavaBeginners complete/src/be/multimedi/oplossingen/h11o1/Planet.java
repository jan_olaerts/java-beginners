package be.multimedi.oplossingen.h11o1;

public enum Planet {
    MERCURY(3.3E23, 0.4),
    VENUS(4.86E24,0.7),
    EARTH(5.97E24,1),
    MARS(6.41E23,1.5),
    JUPITER(1.89E27,5.2),
    SATURN(5.68E26,9.5),
    URANUS(6.68E25,19.2),
    NEPTUNE(1.02E26,30.1);
    //PLUTO NOOOOOO, WHY?!!!!!!

    private double mass;
    private double distance; // in AU, astronomical units

    Planet(double mass, double distance) {
        this.mass = mass;
        this.distance = distance;
    }

    public double getMass(){
        return mass;
    }

    public double getDistance(){
        return distance;
    }
}
