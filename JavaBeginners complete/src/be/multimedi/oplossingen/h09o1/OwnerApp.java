package be.multimedi.oplossingen.h09o1;

public class OwnerApp {
    public static void main(String[] args) {
        Owner owner = new Owner("Ward Truyen");
        Pet pet = new Pet("tijgertje");
        owner.setPet(pet);

        System.out.println("Owner: " + owner.getName());
        System.out.println("Pet: " + owner.getPet().getName());
    }
}
