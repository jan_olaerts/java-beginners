package be.multimedi.oplossingen.h09o1;

public class HouseApp {
    public static void main(String[] args) {
        House woning = new House();
        System.out.println("House:");
        System.out.println(woning.getBathRoom().getName());
        System.out.println(woning.getLivingRoom().getName());
        System.out.println(woning.getKitchen().getName());
        System.out.println(woning.getSleepingRoom().getName());
    }
}
