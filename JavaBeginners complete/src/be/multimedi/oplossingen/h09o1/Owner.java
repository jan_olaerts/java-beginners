package be.multimedi.oplossingen.h09o1;

public class Owner {
    private String name;
    private Pet pet;

    public Owner(String name){
        this.name = name;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getName() {
        return name;
    }
}
