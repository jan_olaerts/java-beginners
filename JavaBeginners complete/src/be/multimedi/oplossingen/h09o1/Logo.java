package be.multimedi.oplossingen.h09o1;

public class Logo {
    private Rectangle r;
    private Circle c;
    private String t;

    public Rectangle getRectangle() {
        return r;
    }

    public void setRectangel(Rectangle r) {
        this.r = r;
    }

    public Circle getCircle() {
        return c;
    }

    public void setCircle(Circle c) {
        this.c = c;
    }

    public String getString() {
        return t;
    }

    public void setString(String t) {
        this.t = t;
    }

    public Logo(Rectangle r, Circle c, String t){
        setRectangel(r);
        setCircle(c);
        setString(t);
    }

    public double getArea(){
        return r.getArea() + c.getArea();
    }
}
