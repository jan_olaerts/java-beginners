package be.multimedi.oplossingen.h09o1;

public class Room {
    private String name;

    public Room(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
