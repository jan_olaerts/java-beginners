package be.multimedi.oplossingen.h09o1.logo;

public class Logo {
    private Rectangle rect;
    private Circle circle;
    private String text;

    public Rectangle getRectangle() {
        return rect;
    }

    public void setRectangle(Rectangle r) {
        this.rect = r;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle c) {
        this.circle = c;
    }

    public String getString() {
        return text;
    }

    public void setString(String t) {
        this.text = t;
    }

    public Logo(Rectangle rect, Circle circle, String text){
        setRectangle(rect);
        setCircle(circle);
        setString(text);
    }

    public double getArea(){
        return rect.getArea() + circle.getArea();
    }
}
