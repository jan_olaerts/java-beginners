package be.multimedi.oplossingen.h09o1.logo;

public class LogoArrayApp {
    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(10,11,12,13);
        Circle c1 = new Circle(20, 21, 22);
        Rectangle r2 = new Rectangle(30,31,32,33);
        Circle c2 = new Circle(40, 41, 42);
        String t = "Hello World";
        LogoArray l = new LogoArray(t);
        l.addRectangle(r1);
        l.addRectangle(r2);
        l.addCircle(c1);
        l.addCircle(c2);

        System.out.println("Logo shapes Area:" + l.getArea());
    }
}
