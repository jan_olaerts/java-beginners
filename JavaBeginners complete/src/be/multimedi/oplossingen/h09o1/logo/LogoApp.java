package be.multimedi.oplossingen.h09o1.logo;

public class LogoApp {
    public static void main(String[] args) {
        Rectangle r = new Rectangle(10,11,12,13);
        Circle c = new Circle(20, 21, 22);
        String t = "Hello World";
        Logo l = new Logo(r, c, t);

        System.out.println("Logo shapes Area:" + l.getArea());
    }
}
