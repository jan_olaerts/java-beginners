package be.multimedi.oplossingen.h09o1.logo;

public class LogoArray {
    private static final int LIST_MAX = 128;
    private Rectangle[] rectangles = new Rectangle[LIST_MAX];
    private int rectCount = 0;
    private Circle[] circles = new Circle[LIST_MAX];
    private int circleCount = 0;
    private String text;

    public LogoArray( String text) {
        this(null,null, text);
    }

    public LogoArray(Rectangle rect, Circle circle, String text){
        addRectangle(rect);
        addCircle(circle);
        setString(text);
    }

    public void addRectangle(Rectangle rect){
        if(rectCount<LIST_MAX-1 && rect != null){
            rectangles[rectCount] =  rect;
            rectCount++;
        }
    }

    public void addCircle(Circle circle){
        if(circleCount<LIST_MAX-1 && circle != null){
            circles[circleCount] =  circle;
            circleCount++;
        }
    }

    public String getString() {
        return text;
    }

    public void setString(String t) {
        this.text = t;
    }

    public double getArea(){
        double total = 0;
        for(int i = 0; i<rectCount; i++){
            total += rectangles[i].getArea();
        }
        for(int i = 0; i<rectCount; i++){
            total += circles[i].getArea();
        }
        return total;
    }
}
