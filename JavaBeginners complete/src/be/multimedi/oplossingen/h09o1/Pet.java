package be.multimedi.oplossingen.h09o1;

public class Pet {
    private String name;

    public String getName() {
        return name;
    }

    public Pet(String name) {
        this.name = name;
    }
}
