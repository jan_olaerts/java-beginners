package be.multimedi.oplossingen.h09o1;

import java.util.Scanner;

public class HigherLowerApp {
    public static void main(String[] args) {
        HigherLowerGame game = new HigherLowerGame(10);
        Scanner keyb = new Scanner(System.in);
        boolean gameRunning = true;
        while (gameRunning) {
            System.out.print("Enter a number between 0 and " + game.getMax() + " : " );
            int guess = keyb.nextInt();
            int result = game.guess(guess);
            if (result == 0) {
                System.out.println("Congratulations!");
                gameRunning = false;
            } else if (result < 0) {
                System.out.println("Higher");
            } else {
                System.out.println("Lower");
            }
        }
    }
}
