package be.multimedi.oplossingen.h10o08;

public class ShapeMain {
    public static void main(String[] args) {
        Rectangle rect = new Rectangle(10, 20, 30, 40);
        Square square = new Square(10, 20, 30);
        Triangle triangle = new Triangle(10, 20, 30, 40, 50);
        Circle circle = new Circle(10, 20, 30);

        Rectangle rect2 = new Rectangle(rect);
        Square square2 = new Square(10, 20, 30);
        Triangle triangle2 = new Triangle(20, 30, 40, 50, 60);
        Circle circle2 = new Circle(10, 20, 31);

        Shape[] shapes = {rect, square, triangle, circle, rect2, square2, triangle2, circle2};

        for (Shape s : shapes) {
            System.out.println(s);
            System.out.format("\t%s: %d\n", "hashCode", s.hashCode());
            for (int i = 0; i < shapes.length; i++) {
                System.out.format("\tthis.equals(shapes[%d]): %b\n", i, s.equals(shapes[i]));
            }
        }
    }
}
