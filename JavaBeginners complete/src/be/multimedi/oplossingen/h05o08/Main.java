package be.multimedi.oplossingen.h05o08;

public class Main {
    public static void main(String[] args){
        int number1 = 1;
        int number2 = 1;
        int number3 = -5;
        System.out.println("number1\t: " + number1);
        System.out.println("number2\t: " + number2);
        System.out.println("number3\t: " + number3);
        System.out.println("number3 << number2\t: " + (number3 << number2));
        System.out.println("number1 << number2\t: " + (number1 << number2));
        System.out.println("number1 >>> number2\t: " + (number1 >>> number2));
        System.out.println("number3 >>> number2\t: " + (number3 >>> number2));
        System.out.println("number1 >> number3\t: " + (number1 >> number3));
        System.out.println("      1 << 31\t\t: " + (1 << 31));
    }
}
