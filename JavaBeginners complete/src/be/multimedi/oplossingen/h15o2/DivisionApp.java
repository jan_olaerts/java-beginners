package be.multimedi.oplossingen.h15o2;

import java.util.Scanner;

public class DivisionApp {
    public static void main(String[] args){
        try {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("num: ");
            int num = Integer.parseInt(keyboard.next());
            System.out.print("den: ");
            int den = Integer.parseInt(keyboard.next());
            int div = num / den;
            System.out.format("result: %d/%d=%d", num, den, div);
            keyboard.close();
        }catch(NumberFormatException nfe){
            //# bij het ingeven van iets wat geen nummer is
            System.out.println("Invalid number!");
            System.out.println(nfe.getMessage());
            nfe.printStackTrace();
        }/* Extra
        catch(Exception e){
            //# bij het delen door nul
            System.out.println("Exception!");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }// */
    }
}
