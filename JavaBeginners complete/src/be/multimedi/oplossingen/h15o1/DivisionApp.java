package be.multimedi.oplossingen.h15o1;

import java.util.Scanner;

public class DivisionApp {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("number: ");
        int num = Integer.parseInt(keyboard.next());
        System.out.print("denominator: ");
        int den = Integer.parseInt(keyboard.next());
        int div = num / den;
        System.out.format("result: %d/%d=%d", num, den, div);
        keyboard.close();
    }
}
