package be.multimedi.oplossingen.h04o2;

import java.util.Random;
import java.util.Scanner;

public class HigherLowerApp {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();
        int numberToGuess = rand.nextInt(100);
        boolean guessed = false;
        while( !guessed ){
            System.out.print("Enter a number: ");
            int guess = keyboard.nextInt();
            if( guess < numberToGuess){
                System.out.println("Higher!");
            }else {
                if (guess > numberToGuess) {
                    System.out.println("Lower!");
                } else {
                    System.out.println("Guessed");
                    guessed = true;
                }
            }
        }
    }
}
