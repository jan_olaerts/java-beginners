package be.multimedi.oplossingen.h10o06;

public class SquareApp {
    public static void main(String[] args) {
        Square square1 = new Square();
        Square square2 = new Square(10);
        Square square3 = new Square(10,11,12);
        Square square4 = new Square(square3);
        Rectangle rect1 = new Rectangle();
        Rectangle rect2 = new Rectangle();
        System.out.println("Square count\t: " + Rectangle.getCount());
        System.out.println("Rectangle count\t: " + Square.getCount());
    }

    public static void printSquare(String title, Square square){
        System.out.println(title + ":");
        System.out.println(" X\t: " + square.getX());
        System.out.println(" Y\t: " + square.getY());
        System.out.println(" Width\t: " + square.getWidth());
        System.out.println(" Height\t: " + square.getHeight());
        System.out.println(" Descr\t: " + square.DESCRIPTION);
        System.out.println(" Area\t: " + square.getArea());
    }
}
