package be.multimedi.oplossingen.h10o06;

public class Square extends Rectangle {
    private static int count = 0;
    public final String DESCRIPTION = super.DESCRIPTION + " (Square)";

    public Square() {
        this(0, 0, 0);
    }

    public Square(int side) {
        this(side, 0, 0);
    }

    public Square(int side, int x, int y) {
        setSide(side);
        setPosition(x, y);
        count++;
    }

    public Square(Square s) {
        this(s.getSide(), s.getX(), s.getY());
    }

    public void setSide(int side) {
        super.setWidth(side);
        super.setHeight(side);
    }

    @Override
    public void setHeight(int height) {
        setSide(height);
    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    public int getSide() {
        return getHeight();
    }

    public static int getCount(){
        return count;
    }
}
