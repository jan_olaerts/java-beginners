package be.multimedi.oplossingen.h13o3;

public class PigMain {
   public static void main(String[] args) {
      Pig p = new MyPig();
      p.grunt();
      p.fly();
   }
}
