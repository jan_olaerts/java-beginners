package be.multimedi.oplossingen.h13o3;

public interface Pig {
   public void grunt();

   public default void fly() {
      grunt();
      System.out.println("The pig floats up into the air!");
      grunt();
      System.out.println("The pig crashes to the ground!");
      grunt();
   }
}
