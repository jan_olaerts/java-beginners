package be.multimedi.oplossingen.h04o4x;

public class KoekjesApp {
    public static void main(String[] args) {
        System.out.println("Ik neem de koekjesdoos uit de kast");
        for (int teller = 0; teller < 5; teller++){
            System.out.println("Ik eet koekje nr " + teller);
        }
        System.out.println("Ik leg de koekjesdoos terug in de kast");
    }
}
