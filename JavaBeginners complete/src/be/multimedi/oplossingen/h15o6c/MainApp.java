package be.multimedi.oplossingen.h15o6c;

import be.multimedi.oplossingen.h15o6.Rectangle;

public class MainApp {
   // Met throw RuntimeException
   public static void main(String[] args) {
      try {
         test();
      }catch (RuntimeException re) {
         System.out.println("In main(...) Exception");
         System.out.println(re.getMessage());
         re.printStackTrace();
      }
      System.out.println("main(...) End");
   }

   public static void test() {
      try {
         var rect = new Rectangle();
         rect.setHeight(-1);
      }catch (RuntimeException re) {
         System.out.println("In test() Exception");
         System.out.println(re.getMessage());
         re.printStackTrace();
         throw re;
      }finally {
         System.out.println("In test() Finally");
      }
      System.out.println("test() end");
   }
}
