package be.multimedi.oplossingen.h10o04;

public class Square extends Rectangle {
    public static String DESCRIPTION = "Square";

    public void setSide( int side ){
        super.setWidth(side);
        super.setHeight(side);
    }

    @Override
    public void setHeight(int height) {
        setSide(height);
    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    public int getSide(){
        return getHeight();
    }
}
