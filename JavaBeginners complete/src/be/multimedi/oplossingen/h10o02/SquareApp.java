package be.multimedi.oplossingen.h10o02;

public class SquareApp {
    public static void main(String[] args) {
        Square square = new Square();
        square.setPosition(10,20);
        square.setHeight(30);
        square.setWidth(40);
        System.out.println("X\t: " + square.getX());
        System.out.println("Y\t: " + square.getY());
        System.out.println("Width\t: " + square.getWidth());
        System.out.println("Height\t: " + square.getHeight());
        System.out.println("Area\t: " + square.getArea());
        System.out.println("Perimeter\t: " + square.getPerimeter());
        System.out.println("Descr\t: " + square.DESCRIPTION);
    }
}
