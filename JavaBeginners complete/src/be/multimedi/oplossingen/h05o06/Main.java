package be.multimedi.oplossingen.h05o06;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("number1: ");
        int number1 = keyboard.nextInt();
        System.out.print("number2: ");
        int number2 = keyboard.nextInt();
        System.out.println("number1 < number2\t: " +  (number1 < number2));
        System.out.println("number1 <= number2\t: " +  (number1 <= number2));
        System.out.println("number1 > number2\t: " +  (number1 > number2));
        System.out.println("number1 >= number2\t: " +  (number1 >= number2));
        System.out.println("number1 == number2\t: " +  (number1 == number2));
        System.out.println("number1 != number2\t: " +  (number1 != number2));
    }
}
