package be.multimedi.oplossingen.h12o1;

import java.util.Scanner;

public class WrappersAppExceptionHandled {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int number;
        do {
            System.out.print("Enter a number: ");
            String input = keyboard.next();
            try {
                number = Integer.parseInt(input);
                break;
            } catch (NumberFormatException nfe) {
                System.out.println(nfe);
                System.out.println("Try again!");
            }
        } while (true);
        number += 10;
        System.out.println("number: " + number);
        System.out.println("Interger bits: " + Integer.BYTES * 8);
        System.out.println("Interger bytes: " + Integer.BYTES);
        System.out.println("Integer min value: " + Integer.MIN_VALUE);
        System.out.println("Integer max value: " + Integer.MAX_VALUE);

        System.out.println("Short bits: " + Short.BYTES * 8);
        System.out.println("Short bytes: " + Short.BYTES);
        System.out.println("Short min value: " + Short.MIN_VALUE);
        System.out.println("Short max value: " + Short.MAX_VALUE);

        System.out.println("Byte bits: " + Byte.BYTES * 8);
        System.out.println("Byte bytes: " + Byte.BYTES);
        System.out.println("Byte min value: " + Byte.MIN_VALUE);
        System.out.println("Byte max value: " + Byte.MAX_VALUE);

        System.out.println("Character bits: " + Character.BYTES * 8);
        System.out.println("Character bytes: " + Character.BYTES);
        System.out.println("Character min value: " + (int) Character.MIN_VALUE);
        System.out.println("Character max value: " + (int) Character.MAX_VALUE);

        System.out.println("Float bits: " + Float.BYTES * 8);
        System.out.println("Float bytes: " + Float.BYTES);
        System.out.println("Float min value: " + Float.MIN_VALUE);
        System.out.println("Float max value: " + Float.MAX_VALUE);

        System.out.println("Double bits: " + Double.BYTES * 8);
        System.out.println("Double bytes: " + Double.BYTES);
        System.out.println("Double min value: " + Double.MIN_VALUE);
        System.out.println("Double max value: " + Double.MAX_VALUE);
    }
}
