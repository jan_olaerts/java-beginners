package be.multimedi.oplossingen.h12o1;

import java.util.Scanner;

public class WrappersApp {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a number: ");
        String input = keyboard.next();
        int number = Integer.parseInt(input);
        Integer wrapper = Integer.valueOf(input);
        number += 10;
        wrapper += 10;
        System.out.println("number: " + number);
        System.out.println("wrapper: " + wrapper);
        System.out.println("Interger bits: " + Integer.BYTES*8);
        System.out.println("Interger bytes: " + Integer.BYTES);
        System.out.println("Integer min value: " + Integer.MIN_VALUE);
        System.out.println("Integer max value: " + Integer.MAX_VALUE);

        System.out.println("Short bits: " + Short.BYTES*8);
        System.out.println("Short bytes: " + Short.BYTES);
        System.out.println("Short min value: " + Short.MIN_VALUE);
        System.out.println("Short max value: " + Short.MAX_VALUE);

        System.out.println("Byte bits: " + Byte.BYTES*8);
        System.out.println("Byte bytes: " + Byte.BYTES);
        System.out.println("Byte min value: " + Byte.MIN_VALUE);
        System.out.println("Byte max value: " + Byte.MAX_VALUE);

        System.out.println("Character bits: " + Character.BYTES*8);
        System.out.println("Character bytes: " + Character.BYTES);
        System.out.println("Character min value: " + (int)Character.MIN_VALUE);
        System.out.println("Character max value: " + (int)Character.MAX_VALUE);

        System.out.println("Float bits: " + Float.BYTES*8);
        System.out.println("Float bytes: " + Float.BYTES);
        System.out.println("Float min value: " + Float.MIN_VALUE);
        System.out.println("Float max value: " + Float.MAX_VALUE);

        System.out.println("Double bits: " + Double.BYTES*8);
        System.out.println("Double bytes: " + Double.BYTES);
        System.out.println("Double min value: " + Double.MIN_VALUE);
        System.out.println("Double max value: " + Double.MAX_VALUE);
    }
}
