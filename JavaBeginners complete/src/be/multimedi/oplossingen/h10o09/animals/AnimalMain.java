package be.multimedi.oplossingen.h10o09.animals;

import java.util.Random;

public class AnimalMain {
    public static final int MAX_ANIMALS = 8;
    public static final int ANIMAL_TYPE_COUNT = 4;

    public static void main(String[] args) {
        Random rand = new Random();
        Animal[] animals = new Animal[MAX_ANIMALS];
        for (int i = 0; i < animals.length; i++) {
            switch (rand.nextInt(ANIMAL_TYPE_COUNT)){
               case 0:
                  animals[i] = new Cat("Minous "+String.valueOf(i));
                  break;
               case 1:
                  animals[i] = new Fish("Shark"+String.valueOf(i));
                  break;
               case 2:
                  animals[i] = new Snake("Apple"+String.valueOf(i));
                  break;
                case 3:
                    animals[i] = new Bird("Tweet"+String.valueOf(i));
                    break;
               default:
                  System.out.println("Error invalid random number");
            }
        }

        for (Animal animal: animals){
           System.out.println("Animal name: " + animal.getName());
           animal.makeNoise();
           animal.move();
           System.out.println();
        }
    }
}
