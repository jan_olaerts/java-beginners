package be.multimedi.oplossingen.h10o09.animals;

public class Bird extends Animal{
    public Bird(){
        super();
    }
    public Bird(String name){
        super(name);
    }

    @Override
    public void move() {
        System.out.println("The bird floats through the air.");
    }

    @Override
    public void makeNoise() {
        System.out.println("Chirp!");
    }
}
