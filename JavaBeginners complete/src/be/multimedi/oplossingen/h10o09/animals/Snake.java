package be.multimedi.oplossingen.h10o09.animals;

public class Snake extends Animal {
    public Snake(){
        super();
    }
    public Snake(String name){
        super(name);
    }

    @Override
    public void move() {
        System.out.println("The snake slithers over the ground!");
    }

    @Override
    public void makeNoise() {
        System.out.println("SSSSS!");
    }
}
