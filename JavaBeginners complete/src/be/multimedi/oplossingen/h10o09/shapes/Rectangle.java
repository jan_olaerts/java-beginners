package be.multimedi.oplossingen.h10o09.shapes;

import java.util.Formatter;

public class Rectangle extends Shape {
    private static int count = 0;
    protected int width;
    protected int height;

    public Rectangle() {
        this(0, 0, 0, 0);
    }

    public Rectangle(int width, int height){
        this(width, height, 0, 0);
    }

    public Rectangle(int width, int height, int x, int y) {
        count++;
        setWidth(width);
        setHeight(height);
        setX(x);
        setY(y);
    }

    public Rectangle( Rectangle r ) {
        this(r.width, r.height, r.x, r.y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width < 0 ? -width : width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height < 0 ? -height : height;
    }

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public double getPerimeter() {
        return (width + height) * 2;
    }

    @Override
    public void grow(int d) {
        setWidth(width + d);
        setHeight(height + d);
    }

    @Override
    public String toString(){
        Formatter f = new Formatter().format("Rectangle:");
        f.format("\n\tWidth: %d\n\tHeight: %d", width, height);
        f.format("\n\tPosX: %d\n\tPosY: %d\n\tArea: %f" , x, y, getArea());
        return f.toString();
    }

    @Override
    public int hashCode() {
        return getX()*7 + getY()*13 + height*17 + width*19;
    }

    @Override
    public boolean equals(Object o) {
        if(o != null && getClass() == o.getClass()){
            Rectangle r = (Rectangle)o;
            if( (r.getX() == getX()) &&
                    (r.getY() == getY()) &&
                    (r.width == width) &&
                    (r.height == height)){
                return true;
            }
        }
        return false;
    }

    public static int getCount(){
        return count;
    }
}
