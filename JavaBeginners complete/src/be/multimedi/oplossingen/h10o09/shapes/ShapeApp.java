package be.multimedi.oplossingen.h10o09.shapes;

public class ShapeApp {
    public static void main(String[] args) {
        Rectangle rect = new Rectangle(10, 20, 30, 40);
        Square square = new Square(10, 20, 30);
        Triangle triangle = new Triangle(10, 20, 4, 40, 50);
        IsoSeclesTriangle isTriangle = new IsoSeclesTriangle(10,20,30,40);
        Circle circle = new Circle(10, 20, 30);

        Rectangle rect2 = new Rectangle(rect);
        Square square2 = new Square(10, 20, 30);
        Triangle triangle2 = new Triangle(20, 30, 4, 50, 60);
        IsoSeclesTriangle isTriangle2 = new IsoSeclesTriangle(20,30,40,50);
        Circle circle2 = new Circle(10, 20, 31);

        Shape[] shapes = {rect, square, triangle, isTriangle, circle, rect2, square2, triangle2, isTriangle, circle2, new Rectangle(square), new Square(10)};

        for (Shape s : shapes) {
//            System.out.println(s.toString());
            printShape(s);
        }
    }

    public static void printShape(Shape shape) {
        System.out.println( " getClass(): "+ shape.getClass());
        System.out.println( " getClass().getSimpleName(): "+ shape.getClass().getSimpleName());
        System.out.println( " Area      : " + shape.getArea() );
        System.out.println( " Perimeter : " + shape.getPerimeter() );
        System.out.println( " Position  : (" + shape.getX() + ", " + shape.getY() + ")" );
        if ( shape instanceof Square) {
            Square sqr = (Square) shape;
            System.out.println(" side      : " + sqr.getSide());
        } else if (shape instanceof Rectangle) {
            Rectangle rect = (Rectangle)shape;
            System.out.println(" width     : " + rect.getWidth());
            System.out.println(" height    : " + rect.getHeight());
        } else if ( shape instanceof Triangle ){
            Triangle tri = (Triangle)shape;
            System.out.println(" width     : " + tri.getWidth());
            System.out.println(" height    : " + tri.getHeight());
            System.out.println(" perpendicular : " + tri.getPerpendicular());
        } else if ( shape instanceof Circle ){
            Circle circ = (Circle)shape;
            System.out.println(" radius    : " + circ.getRadius());
        } else {
            System.out.println("Unkown shape?");
        }
        System.out.println();
    }
}
