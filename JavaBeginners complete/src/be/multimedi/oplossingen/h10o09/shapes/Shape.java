package be.multimedi.oplossingen.h10o09.shapes;

public abstract class Shape {
    private static int count = 0;
    protected int x;
    protected int y;

    public Shape(){
        this(0, 0);
    }
    public Shape(int x, int y){
        count++;
        setX(x);
        setY(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setPosition(int x, int y){
       setX(x);
       setY(y);
    }

    public void grow(int d){

    }

    public static int getCount(){
        return count;
    }

    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract int hashCode();
    public abstract boolean equals(Object o);

    @Override
    public String toString() {
        return "Shape{" +
                "x=" + x +
                ", y=" + y +
                ", Area=" + getArea() +
                '}';
    }
}
