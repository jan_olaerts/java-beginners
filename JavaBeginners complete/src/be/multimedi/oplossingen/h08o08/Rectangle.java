package be.multimedi.oplossingen.h08o08;

public class Rectangle {
    private int x;
    private int y;
    private int height;
    private int width;

    public Rectangle(){
       this(0,0);
    }

    public Rectangle(int width, int height){
       this(width, height, 0 , 0);
    }

    public Rectangle(int width, int height, int x, int y){
        this.setWidth(width);
        this.setHeight(height);
        this.setPosition(x,y);
    }

    public Rectangle( Rectangle r ){
        this(r.width, r.height, r.x, r.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setHeight(int height) {
        this.height = height<0?-height:height;
    }

    public void setWidth(int width) {
        this.width = width<0?-width:width;
    }

    public void setPosition( int x, int y){
        setX(x);
        setY(y);
    }

    public void grow(int d){
        setWidth(width+d);
        setHeight(height+d);
    }

    public double getArea(){
        return (double)height*width;
    }

    public double getPerimeter(){
        return (double)width*2+height*2;
    }
}
