package be.multimedi.oplossingen.h06o4;

public class StringBuildApp {
    public static void main(String[] args) {
        StringBuilder sb1 = new StringBuilder("This is my first line of text.");
        StringBuilder sb2 = new StringBuilder("This is my second line of text.");
        System.out.println("sb1\t: " + sb1);
        System.out.println("sb2\t: " + sb2);
        sb1.append(" And this is what I added.");
        sb2.reverse();
        System.out.println("sb1\t: " + sb1);
        System.out.println("sb2\t: " + sb2);
        sb1 = new StringBuilder(sb1.toString().replaceAll(" ", ""));
        sb2 = new StringBuilder(sb2.toString().replaceAll("t", "tt"));
        System.out.println("sb1\t: " + sb1);
        System.out.println("sb2\t: " + sb2);
    }
}
