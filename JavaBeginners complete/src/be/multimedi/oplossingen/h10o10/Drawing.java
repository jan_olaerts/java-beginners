package be.multimedi.oplossingen.h10o10;

public class Drawing {
    public static int MAX = 4;
    private int listSizeMultiplyer = 1;
    private Shape[] shapes = new Shape[MAX];
    private int size = 0;

    public void add(Shape shape) {
        if (shape == null || size >= shapes.length || locate(shape) != -1) {
            if (size >= shapes.length) {
                System.out.println("List full, resizing");
                Shape[] tempShapes = new Shape[MAX*(++listSizeMultiplyer)];
                int i = 0;
                for(Shape s:shapes){
                    tempShapes[i++] = s;
                }
                shapes = tempShapes;
                System.out.println("List size: " + shapes.length);
            } else {
                System.out.println("Not adding shape: " + shape);
                if (shape != null) {
                    System.out.println("The shape is already in the list");
                }
                return;
            }
        }
        shapes[getFreeLocation()] = shape;
        size++;
    }

    public boolean isPersent(Shape shape) {
        return shape != null && locate(shape) != -1;
    }

    private int locate(Shape shape) {
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] == shape) {
                return i;
            }
        }
        return -1;
    }

    public void remove(Shape shape) {
        int l = locate(shape);
        if (l > 0) {
            shapes[l] = null;
            size--;
        } else {
            System.out.println("shape was never in drawing to begin with: " + shape);
        }
    }

    private int getFreeLocation() {
        return locate(null);
    }

    public void clear() {
        for (Shape s : shapes) s = null;
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public void draw(){
        System.out.println("Drawing: ");
        for (Shape s : shapes){
            if(s!=null){
                System.out.println(s);
                //s.draw();
            }
        }
    }
}
