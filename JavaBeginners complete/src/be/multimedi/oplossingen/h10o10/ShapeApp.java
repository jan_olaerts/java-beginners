package be.multimedi.oplossingen.h10o10;

public class ShapeApp {
    public static void main(String[] args) {
        Rectangle rect = new Rectangle(10, 20, 30, 40);
        Square square = new Square(10, 20, 30);
        Triangle triangle = new Triangle(10, 20, 30, 40, 50);
        IsoSeclesTriangle isTriangle = new IsoSeclesTriangle(10,20,30,40);
        Circle circle = new Circle(10, 20, 30);

        Rectangle rect2 = new Rectangle(rect);
        Square square2 = new Square(10, 20, 30);
        Triangle triangle2 = new Triangle(20, 30, 40, 50, 60);
        IsoSeclesTriangle isTriangle2 = new IsoSeclesTriangle(20,30,40,50);
        Circle circle2 = new Circle(10, 20, 31);

        Drawing d = new Drawing();
        d.add(rect);
        printCount(d);
        d.add(square);
        printCount(d);
        d.add(triangle);
        printCount(d);
        d.add(isTriangle);
        printCount(d);
        d.add(circle);
        printCount(d);

        d.add(rect2);
        printCount(d);
        d.add(square2);
        printCount(d);
        d.add(triangle2);
        printCount(d);
        d.add(isTriangle2);
        printCount(d);
        d.add(circle2);
        printCount(d);

        d.add(rect);
        printCount(d);
        d.add(square);
        printCount(d);
        d.add(triangle);
        printCount(d);
        d.add(isTriangle);
        printCount(d);
        d.add(circle);
        printCount(d);

        d.draw();
    }

    public static void printCount(Drawing d){
        System.out.println( "d.size = " + d.getSize());
    }
}
