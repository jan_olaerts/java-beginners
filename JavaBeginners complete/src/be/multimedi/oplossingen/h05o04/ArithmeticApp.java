package be.multimedi.oplossingen.h05o04;

import java.util.Scanner;

public class ArithmeticApp {
    public static void main(String[] args){
        // Enter number1 as 100 and number2 as 20 for a good and easy sample
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter number1: ");
        int number1 = keyboard.nextInt();
        System.out.print("Enter number2: ");
        int number2 = keyboard.nextInt();
        System.out.println("Sum\t\t\t: " + (number1 + number2));
        System.out.println("Difference\t: " + (number1 - number2));
        System.out.println("Product\t\t: " + (number1 * number2));
        System.out.println("Division\t: " + (number1 / number2));
        System.out.println("Rest\t\t: " + (number1 % number2));
        System.out.println("--number1\t: " + (--number1));
        System.out.println("number1--\t: " + (number1--));
        System.out.println("++number1\t: " + (++number1));
        System.out.println("number1++\t: " + (number1++));
        System.out.println("number1\t: " + (number1));
        System.out.println("***");

        char aChar = 'a';
        System.out.println("aChar\t\t: " + (aChar));
        System.out.println("aChar + 5\t: " + (aChar + 5));
        System.out.println("(char)(aChar + 5)\t: " + (char)(aChar + 5));
        System.out.println("aChar++\t\t: " + (aChar++));
        System.out.println("++aChar\t\t: " + (++aChar));
    }
}
