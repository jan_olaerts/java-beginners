package be.multimedi.oplossingen.h14o2;

public class Musician {

   public void play() {
      class Instrument {
         public void makeSound() {
            System.out.println("drums rumbling");
         }
      }
      Instrument i = new Instrument();
      i.makeSound();
   }
}
