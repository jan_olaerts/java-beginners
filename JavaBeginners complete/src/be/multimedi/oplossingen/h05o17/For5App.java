package be.multimedi.oplossingen.h05o17;

public class For5App {
    public static void main(String[] args){
        for(int i = -10; i<=10; i++){
            if( i <= 0) {
                System.out.println(i);
            }else{
                System.out.println("+" + i);
            }
        }
    }
}
