package be.multimedi.oplossingen.h05o17;

public class For4App {
    public static void main(String[] args){
        for(char c = 'z'; c >= 'a'; c--){
            System.out.print(c);
        }
    }
}
