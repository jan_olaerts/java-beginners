package be.multimedi.oplossingen.h05o17;

public class For8App {
    public static void main(String[] args) {
        int totalFound = 0;
        for(int n = 2; n<1000; n++){
            boolean p = true;
            for(int i = 2; i<n && p==true; i++){
               if( n%i==0){
                   p = false;
               }
            }
            if(p==true){
                System.out.println("n: " + n);
                totalFound++;
            }
        }
        System.out.println("Total: " + totalFound);
    }
}
