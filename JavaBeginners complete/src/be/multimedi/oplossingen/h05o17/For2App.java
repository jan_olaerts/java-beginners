package be.multimedi.oplossingen.h05o17;

public class For2App {
    public static void main(String[] args) {
        final int MULTIPLE = 7;
        final int MAX = 200;
        for (int i = 0; i * MULTIPLE < MAX; i++) {
            System.out.println(i * MULTIPLE);
        }
    }
}
