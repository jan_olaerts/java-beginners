package be.multimedi.oplossingen.h05o17;

public class For6App {
    public static void main(String[] args) {
        for(int i = -10; i<=10; i++){
            System.out.println(i<=0?i:"+"+i);
        }
    }
}
