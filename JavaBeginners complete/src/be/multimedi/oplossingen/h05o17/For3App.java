package be.multimedi.oplossingen.h05o17;

public class For3App {
    public static void main(String[] args) {
        final int POWER = 11;
        final int MAX = 100_000;
        int value = 1;
        for (int i = 0; value < MAX; i++) {
            System.out.println(value);

            // next power of 11
            value = POWER;
            for (int j = 0; j < i; j++) {
                value *= POWER;
            }
        }
    }
}
