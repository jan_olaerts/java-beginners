package be.multimedi.oplossingen.h14o4;

public class ShapeMain {
   public static void main(String[] args) {
      Rectangle rect = new Rectangle(10, 20, 30, 40);
      Square square = new Square(10, 20, 30);
      Triangle triangle = new Triangle(10, 20, 30, 40, 50);
      Circle circle = new Circle(10, 20, 30);

      Drawing drawing = new Drawing();
      drawing.add(rect);
      drawing.add(square);
      drawing.add(triangle);
      drawing.add(circle);

      System.out.println(">> for-each in drawing -> sout");
      for (Drawable drawable : drawing) {
         System.out.println(drawable);
      }

      System.out.println(">> for-each in drawing -> TextDrawingContext");
      TextDrawingContext tdc = new TextDrawingContext();
      for (Drawable drawable : drawing) {
         drawable.draw(tdc);
      }
   }
}
