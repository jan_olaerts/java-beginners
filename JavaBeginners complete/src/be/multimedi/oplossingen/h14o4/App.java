package be.multimedi.oplossingen.h14o4;

import java.util.Formatter;

public class App {
    public static void main(String[] args) {
        Drawing drawing = new Drawing();
        Rectangle rect = new Rectangle(10, 10,1,2);
        Square squa = new Square(40,11,12);
        Triangle tria = new Triangle(10, 20, 30,21,22);
        Circle circ = new Circle(20,31,32);

        drawing.add(rect);
        drawing.add(tria);
        drawing.add(squa);
        drawing.add(circ);
        drawing.scaleDouble();
        System.out.println(drawing.toString());

        System.out.println("** scale triangle and circle **");
        tria.scaleDouble();
        circ.scaleHalf();
        System.out.println("** remove rectange and square **");
        drawing.remove(rect);
        drawing.remove(squa);
        System.out.println(drawing.toString());

        System.out.println("** iterate drawing **");
        for(Drawable drawable:drawing){
            System.out.println(drawable);
        }
        System.out.println("** clear drawing **");
        drawing.clear();
        System.out.println(drawing.toString());
        printShapeCount();

    }

    public static void printShapeCount(){
        Formatter f = new Formatter();
        f.format("Shapes: %d", Shape.getCount());
        f.format("\n Rectangles: %d", Rectangle.getCount());
        f.format("\n Squares: %d", Square.getCount());
        f.format("\n Circles: %d", Circle.getCount());
        f.format("\n Triangles: %d", Triangle.getCount());
        System.out.println(f.toString());
    }
}
