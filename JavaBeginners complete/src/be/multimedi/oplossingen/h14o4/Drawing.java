package be.multimedi.oplossingen.h14o4;

import java.util.Formatter;
import java.util.Iterator;

public class Drawing implements Drawable, Iterable<Drawable> {
    static final int LIST_MAX_SIZE = 16;
    int size;
    Drawable[] list = new Drawable[LIST_MAX_SIZE];

    public void remove(Drawable d) {
        // look for d
        for (int i = 0; i < size; i++) {
            // is this the one?
            if (d == list[i]) {
                // move all the others 1 step forward
                for (int j = i + 1; j < size && j < list.length; j++) {
                    list[j - 1] = list[j];
                }
                // remove the last (double)reference an decrease size
                list[size--] = null;
                return;
            }
        }
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            list[i] = null;
        }
        size = 0;
    }

    public void add(Drawable d) {
        if (size >= LIST_MAX_SIZE) {
            return;
        }
        list[size] = d;
        size++;
    }

    public int getSize() {
        return size;
    }

    @Override
    public void draw(DrawingContext dc) {
        System.out.println("Drawing: ");
        for (Drawable d : list){
            if(d!=null){
                d.draw(dc);
                //s.draw();
            }
        }
    }

    @Override
    public String toString() {
        Formatter f = new Formatter();
        f.format("Drawing: %d/%d", size, LIST_MAX_SIZE);

        for(int i = 0; i<size; i++){
            Drawable d = list[i];
            if (d == null) break;
            f.format("\n %d>%s", i+1, d.toString());
        }
        return f.toString();
    }

    @Override
    public void scale(int factor) {
        for(int i = 0; i<size; i++){
            list[i].scale(factor);
        }
    }

    @Override
    public Iterator<Drawable> iterator() {
        return new DrawableIterator(this);
    }
}
