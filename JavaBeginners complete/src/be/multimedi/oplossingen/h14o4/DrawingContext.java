package be.multimedi.oplossingen.h14o4;

public interface DrawingContext {
    public void draw(Rectangle rect);
    public void draw(Circle circle);
    public void draw(Triangle triangle);
}
