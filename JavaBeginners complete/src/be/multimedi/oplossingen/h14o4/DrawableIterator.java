package be.multimedi.oplossingen.h14o4;

import java.util.Iterator;

public class DrawableIterator implements Iterator<Drawable> {
   Drawing d;
   int index = 0;

   public DrawableIterator(Drawing d) {
      this.d = d;
   }

   @Override
   public boolean hasNext() {
      return index < d.size;
   }

   @Override
   public Drawable next() {
      return d.list[index++];
   }
}