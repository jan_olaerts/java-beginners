package be.multimedi.oplossingen.h14o4;

public interface Drawable extends Scaleable {
    public void draw(DrawingContext dc);
}
