package be.multimedi.oplossingen.h10o07.animals;

public class Cat extends Animal {
    public Cat() {
        super();
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println("The cat runs.");
    }

    @Override
    public void makeNoise() {
        System.out.println("Miaauw!");
    }
}
