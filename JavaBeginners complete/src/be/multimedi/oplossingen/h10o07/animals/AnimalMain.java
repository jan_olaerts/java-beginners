package be.multimedi.oplossingen.h10o07.animals;

public class AnimalMain {
    public static void main(String[] args){
       Cat c = new Cat("Minous");
       Fish f = new Fish("Johny");
       Bird b = new Bird("Charlie");
       Snake s = new Snake("Gerard");

       c.makeNoise();
       c.move();

       f.makeNoise();
       f.move();

       b.makeNoise();
       b.move();

       s.makeNoise();
       s.move();
    }
}
