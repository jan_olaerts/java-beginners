package be.multimedi.oplossingen.h10o07;

public abstract class Animal {
    public String name;

    public Animal(){
        this("");
    }

    public Animal(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void move();
    public abstract void makeNoise();
}
