package be.multimedi.oplossingen.h10o07.shapes;

import java.util.Formatter;

public class Circle extends Shape {
    private static int count = 0;
    private int radius;

    public Circle() {
        this(0);
    }

    public Circle(int radius) {
        this(radius, 0, 0);
    }

    public Circle(int radius, int x, int y) {
        count++;
        setRadius(radius);
        setPosition(x, y);
    }

    public Circle(Circle c) {
        this(c.radius, c.x, c.y);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius < 0 ? -radius : radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }


    @Override
    public void grow(int d) {
        setRadius(radius + d);
    }

    public static int getCount() {
        return count;
    }
}
