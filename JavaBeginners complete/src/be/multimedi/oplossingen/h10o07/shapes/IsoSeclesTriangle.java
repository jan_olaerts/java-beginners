package be.multimedi.oplossingen.h10o07.shapes;

/**
 * Nederlands: Gelijkbenige driehoek /\
 */
public class IsoSeclesTriangle extends Triangle {
    private static int count = 0;

    IsoSeclesTriangle(int x, int y, int w, int h){
        this.setX(x);
        this.setY(y);
        this.setWidth(w);
        this.setHeight(h);
    }

    IsoSeclesTriangle(){
        this(0,0,0,0);
    }

    IsoSeclesTriangle(int w, int h){
        this(0,0,w,h);
    }

    IsoSeclesTriangle(IsoSeclesTriangle copy){
        this(copy.x, copy.y, copy.width, copy.height);
    }

    public static int getCount() {
        return count;
    }

    @Override
    public void setWidth(int width){
        if(width < 0) width = -width;
        this.width = width;
        this.perpendicular = width /2;
    }

    @Override
    public void setPerpendicular(int perpendicular){
        if( perpendicular < 0 ) perpendicular = -perpendicular;
        this.perpendicular = perpendicular;
        this.width = perpendicular * 2;
    }
}
