package be.multimedi.oplossingen.h10o07.shapes;

public class ShapeApp {
    public static void main(String[] args){
        Rectangle rect = new Rectangle(10,20,30,40);
        Square square = new Square(10,20,30);
        Triangle triangle = new Triangle(10,20,4,40,50);
        Circle circle = new Circle(10,20,30);

        Shape[] shapes = {rect, square, triangle, circle};

        int shapeCount = 0;
        for (Shape s : shapes) {
            System.out.println("Shape: " + ++shapeCount);
            System.out.println("  X        : " + s.getX());
            System.out.println("  Y        : " + s.getY());
            System.out.println("  Area     : " + s.getArea());
            System.out.println("  Perimeter: " + s.getPerimeter());
            System.out.println("  Count    : " + s.getCount());
        }
    }
}
