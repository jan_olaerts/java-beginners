package be.multimedi.oplossingen.h10o07;

public class ShapeMain {
    public static void main(String[] args){
        Rectangle rect = new Rectangle(10,20,30,40);
        Square square = new Square(10,20,30);
        Triangle tri = new Triangle(10,20,30,40,50);
        Circle circ = new Circle(10,20,30);

        System.out.println(rect);
        System.out.println(square);
        System.out.println(tri);
        System.out.println(circ);
    }
}
