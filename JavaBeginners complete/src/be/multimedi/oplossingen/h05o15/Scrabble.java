package be.multimedi.oplossingen.h05o15;

import java.io.IOException;

public class Scrabble {
    public static void main(String[] args) throws IOException {
       System.out.print("Enter character: ");
       char c = (char) System.in.read();
       //System.out.println("character\t: " + c);
       int value;
       switch (c){
           case 'a': value = 1; break;
           case 'b': value = 3; break;
           case 'c': value = 3; break;
           case 'd': value = 1; break;
           case 'e': value = 1; break;
           case 'f': value = 5; break;
           case 'g': value = 2; break;
           case 'h': value = 2; break;
           case 'i': value = 1; break;
           case 'j': value = 4; break;
           case 'k': value = 4; break;
           case 'l': value = 2; break;
           case 'm': value = 3; break;
           case 'n': value = 1; break;
           case 'o': value = 1; break;
           case 'p': value = 3; break;
           case 'q': value = 10; break;
           case 'r': value = 1; break;
           case 's': value = 1; break;
           case 't': value = 1; break;
           case 'u': value = 4; break;
           case 'v': value = 4; break;
           case 'w': value = 4; break;
           case 'x': value = 8; break;
           case 'y': value = 8; break;
           case 'z': value = 6; break;
           default: value = 0; break;
       }
       System.out.println("Value\t: " + value);
    }
}
