package be.multimedi.oplossingen.h07o1;

public class ArrayApp {
    public static void main(String[] args){
        int[] numbers = new int[20];
        for( int i = 0; i<20; i++){
            numbers[i] = i*7;
        }

        for( int i: numbers){
            System.out.println(i);
        }

        for( int i = numbers.length -1; i>=0; i--){
            System.out.println(numbers[i]);
        }

        boolean[] booleans = new boolean[4];
        for(int i=0; i< booleans.length; i++){
            booleans[i] = i%2==0;
        }

        for(boolean b : booleans){
            System.out.println(b);
        }
    }
}
