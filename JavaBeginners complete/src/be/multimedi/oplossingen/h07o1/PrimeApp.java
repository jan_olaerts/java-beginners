package be.multimedi.oplossingen.h07o1;

public class PrimeApp {
    public static void main(String[] args) {
        final int BASE = 2;
        final int PRIMES = 50;
        final int PRIME_MAX = 1_000_000;

        int[] primes = new int[PRIMES];
        int value = BASE;
        for (int i = 0; i < PRIMES; i++) {
            boolean notFound = true;
            while (notFound && value < PRIME_MAX) {
                boolean noCheckFailed = true;
                for (int check = value - 1; check >= BASE; check--) {
                    if (value % check == 0) {
                        noCheckFailed = false;
                        break;
                    }
                }
                if (noCheckFailed) {
                    primes[i] = value;
                    notFound = false;
                }
                value++;
            }
        }

        int count = 0;
        for(int i : primes){
            System.out.println(++count + "\t: " + i);
        }
    }
}
