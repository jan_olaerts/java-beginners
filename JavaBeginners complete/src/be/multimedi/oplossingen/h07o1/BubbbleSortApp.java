package be.multimedi.oplossingen.h07o1;

public class BubbbleSortApp {
    static void bubbleSort(int[] numbers) {
        int n = numbers.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (numbers[j - 1] > numbers[j]) {
                    //swap elements
                    temp = numbers[j - 1];
                    numbers[j - 1] = numbers[j];
                    numbers[j] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int numbers[] = {3, 60, 35, 2, 45, 320, 5};

        System.out.println("Array before bubble sort:");
        for (int i : numbers) {
            System.out.print(i + " ");
        }
        System.out.println();

        bubbleSort(numbers);//sorting array elements using bubble sort

        System.out.println("Array after bubble sort:");
        for (int i : numbers) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
