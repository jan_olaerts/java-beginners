package be.multimedi.oplossingen.h07o2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       Scanner keyboard = new Scanner(System.in);
       System.out.print("Enter a line of text: ");
       String text = keyboard.nextLine();

       for(String word: text.split(" ")){
           System.out.println(word);
       }
    }
}
