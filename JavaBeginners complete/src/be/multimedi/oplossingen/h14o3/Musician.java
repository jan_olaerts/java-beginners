package be.multimedi.oplossingen.h14o3;

public class Musician {
    public interface Instrument{
        public void makeSound();
    }

    public void play(){
        Instrument i = new Instrument() {
            @Override
            public void makeSound() {
                System.out.println("Boem boem baf!");
            }
        };
        i.makeSound();
    }
}
