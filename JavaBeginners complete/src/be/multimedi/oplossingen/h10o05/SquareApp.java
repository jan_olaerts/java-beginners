package be.multimedi.oplossingen.h10o05;

public class SquareApp {
    public static void main(String[] args) {
        Square square1 = new Square();
        printSquare("square1", square1);
        Square square2 = new Square(10);
        printSquare("square2", square2);
        Square square3 = new Square(10,11,12);
        printSquare("square3", square3);
        Square square4 = new Square(square3);
        printSquare("square4", square4);
    }

    public static void printSquare(String title, Square square){
        System.out.println(title + ":");
        System.out.println(" X\t: " + square.getX());
        System.out.println(" Y\t: " + square.getY());
        System.out.println(" Width\t: " + square.getWidth());
        System.out.println(" Height\t: " + square.getHeight());
        System.out.println(" Descr\t: " + square.DESCRIPTION);
        System.out.println(" Area\t: " + square.getArea());
    }
}
