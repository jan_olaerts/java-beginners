package be.multimedi.oplossingen.h12o6;

import java.time.Duration;

import java.time.Instant;

public class InstantApp {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Sleeping 2 seconds:");
            Instant iStart = Instant.now();
            Thread.sleep(2000);
            Instant iEnd = Instant.now();
        Duration diff = Duration.between( iStart, iEnd);
        System.out.printf("difference: %d sec %d nano" ,diff.getSeconds(), diff.getNano());
    }
}
