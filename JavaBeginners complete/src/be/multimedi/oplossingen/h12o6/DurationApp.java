package be.multimedi.oplossingen.h12o6;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class DurationApp {
    public static void main(String[] args){
        LocalDate nowDate = LocalDate.now();
        LocalDate birthDate = LocalDate.of(1984, 9, 3);
        Instant now = Instant.now();
        Instant atBirth = Instant.ofEpochSecond(birthDate.toEpochSecond(
                LocalTime.now(), OffsetDateTime.now().getOffset()));

        System.out.println("*** Duration ***");
        Duration duration = Duration.between(atBirth, now);
        System.out.println("Seconds: "+ duration.getSeconds());
        System.out.println("Minutes: "+ duration.getSeconds()/60);
        System.out.println("Hours  : "+ duration.getSeconds()/60/24);

        System.out.println("*** Chrono ***");
        System.out.println("Days   : " + ChronoUnit.DAYS.between(birthDate, nowDate));
        System.out.println("Months : " + ChronoUnit.MONTHS.between(birthDate, nowDate));
        System.out.println("Years  : " + ChronoUnit.YEARS.between(birthDate, nowDate));

        System.out.println("*** Period ***");
        Period period = Period.between(birthDate, nowDate);
        System.out.println("Days   : " + period.getDays());
        System.out.println("Months : " + period.getMonths());
        System.out.println("Years  : " + period.getYears());
    }
}
