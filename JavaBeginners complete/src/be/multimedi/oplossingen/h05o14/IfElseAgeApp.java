package be.multimedi.oplossingen.h05o14;

import java.util.Scanner;

public class IfElseAgeApp {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter your age: ");
        int age = keyboard.nextInt();
        if (age < 6) {
            System.out.println("Baby");
        } else if (age < 12) {
            System.out.println("Kid");
        } else if (age < 18) {
            System.out.println("Teenager");
        } else {
            System.out.println("Adult");
        }
    }
}
