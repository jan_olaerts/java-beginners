package be.multimedi.oplossingen.h05o14;

import java.util.Scanner;

public class BmiApp {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter your weight(kg): ");
        double weight = keyboard.nextDouble();
        System.out.print("Enter your lenght(m): ");
        double lenght = keyboard.nextDouble();
        double bmi =  weight/(lenght*lenght);
        System.out.println("Your BMI: " + bmi);
        if (bmi < 20) {
            System.out.println("  underweight");
        } else if(bmi < 25){
            System.out.println("  ok");
        } else if(bmi < 30){
            System.out.println("  overweight");
        } else if(bmi < 40){
            System.out.println("  obesitas");
        } else {
            System.out.println("  extreme overweight");
        }
    }
}
