package be.multimedi.oplossingen.h12o2;

import java.time.Instant;

public class InstantApp2{
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Sleeping 2 seconds:");
            Instant iStart = Instant.now();
            Thread.sleep(2000);
            Instant iEnd = Instant.now();
        long seconds = iEnd.getEpochSecond() - iStart.getEpochSecond();
        int nanoSeconds = iEnd.getNano() - iStart.getNano();
        System.out.println("difference in time measured:");
        System.out.println("seconds : " + seconds);
        System.out.println("nanosec.: " + nanoSeconds);
    }
}
