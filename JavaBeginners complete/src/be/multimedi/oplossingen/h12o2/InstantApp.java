package be.multimedi.oplossingen.h12o2;

import java.time.Instant;

public class InstantApp {
    public static void main(String [] args){
        Instant now = Instant.now();
        Instant later = now.plusSeconds(7);
        later = later.plusMillis(5);
        later = later.plusNanos(3);
        System.out.println("  now: " + now);
        System.out.println("later: " + later);
    }
}
