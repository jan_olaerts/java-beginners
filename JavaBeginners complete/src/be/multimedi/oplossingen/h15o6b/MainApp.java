package be.multimedi.oplossingen.h15o6b;

public class MainApp {
   // Met throw Exception
   public static void main(String[] args) {
      try {
         test();
      }catch (Exception e) {
         System.out.println("In main(...) Exception");
         System.out.println(e.getMessage());
         e.printStackTrace();
      }
      System.out.println("main(...) End");
   }

   public static void test() throws Exception {
      try {
         var rect = new Rectangle();
         rect.setWidth(-2);
         rect.setHeight(-1);
      }catch (Exception e) {
         System.out.println("In test() Exception");
         System.out.println(e.getMessage());
         e.printStackTrace();
         throw e;
      }finally {
         System.out.println("In test() Finally");
      }
      System.out.println("test() end");
   }
}
