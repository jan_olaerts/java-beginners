package be.multimedi.oplossingen.h15o6b;

import be.multimedi.oplossingen.h15o6.NegativeSizeException;

import java.util.Formatter;

public class Rectangle extends Shape {
    private static int count = 0;
    protected int width;
    protected int height;

    public Rectangle() throws Exception {
        this(0, 0, 0, 0);
    }

    public Rectangle(int width, int height) throws Exception {
        this(width, height, 0, 0);
    }

    public Rectangle(int width, int height, int x, int y) throws Exception  {
        count++;
        setWidth(width);
        setHeight(height);
        setX(x);
        setY(y);
    }

    public Rectangle(Rectangle r ) throws Exception {
        this(r.width, r.height, r.x, r.y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) throws Exception
    {
        if(width<0)
            throw new Exception("Negatieve breedte");
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) throws Exception {
        if(height < 0)
            throw new Exception("Negatieve hoogte");
        this.height = height;
    }

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public double getPerimeter() {
        return (width + height) * 2;
    }

    @Override
    public void grow(int d) throws Exception {
        setWidth(width + d);
        setHeight(height + d);
    }

    @Override
    public String toString(){
        Formatter f = new Formatter().format("Rectangle:");
        f.format("\n\tWidth: %d\n\tHeight: %d", width, height);
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    @Override
    public int hashCode() {
        return getX()*7 + getY()*13 + height*17 + width*19;
    }

    @Override
    public boolean equals(Object o) {
        if(o != null && getClass() == o.getClass()){
            Rectangle r = (Rectangle)o;
            if( (r.getX() == getX()) &&
                    (r.getY() == getY()) &&
                    (r.width == width) &&
                    (r.height == height)){
                return true;
            }
        }
        return false;
    }

    public static int getCount(){
        return count;
    }
}
