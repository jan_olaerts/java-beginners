package be.multimedi.oplossingen.h08o12;

public class Circle {
    public static final int ANGLES = 0;
    private static int count;
    //public static int count = 0;
    private int x;
    private int y;
    private int radius;

    {
        count = 0;
        //ANGLES = 4; //    Error
    }

    public Circle(){
       this(0);
    }

    public Circle(int radius){
       this(radius, 0 , 0);
    }

    public Circle(int radius, int x, int y){
        this.setRadius(radius);
        this.setPosition(x,y);
        count++;
    }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRadius() {
        return radius;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setRadius(int radius) {
        this.radius = radius<0?-radius:radius;
    }

    public void setPosition( int x, int y){
        setX(x);
        setY(y);
    }

    public void grow(int d){
        setRadius(radius+d);
    }

    public double getArea(){
        return Math.PI*radius*radius;
    }

    public double getPerimeter(){
        return 2*Math.PI*radius;
    }

    public static int getCount(){
        return count;
    }
}
