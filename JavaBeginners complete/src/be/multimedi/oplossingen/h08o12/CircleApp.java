package be.multimedi.oplossingen.h08o12;

import static be.multimedi.oplossingen.h08o12.Circle.getCount;

public class CircleApp {
    public static void main(String[] args){
        System.out.println("This program uses a circle");
        Circle circ = new Circle(10,11,12);

        System.out.println("X\t\t: " + circ.getX());
        System.out.println("Y\t\t: " + circ.getY());
        System.out.println("Radius\t: " + circ.getRadius());

        System.out.println("Area\t: " + circ.getArea());
        System.out.println("Perimeter\t: " + circ.getPerimeter());

        System.out.println("Count\t\t: " + getCount());

        System.out.println("Creating 3 Objects of class Circle");
        Circle circ2 = new Circle();
        Circle circ3 = new Circle();
        Circle circ4 = new Circle();
        System.out.println("Count\t\t: " + getCount());
        System.out.println("Angles of a circle: " + Circle.ANGLES);
    }
}
