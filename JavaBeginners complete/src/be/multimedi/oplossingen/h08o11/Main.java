package be.multimedi.oplossingen.h08o11;

public class Main {
    public static void main(String[] args){
        for(float f = 0; f<2*Math.PI; f+=0.1F)
            System.out.format("%.2f rad : %.2f deg : cos = %.2f\n", f, 360F/(2*Math.PI)*f, Math.cos(f) );
    }
}
