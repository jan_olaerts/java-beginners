package be.multimedi.oplossingen.h07o5;

public class Statistics {
    public static int average(int... values){
        if(values.length == 0)
            return 0;
        int total = 0;
        for( int number: values){
            total += number;
        }
        return total/values.length;
    }

    public static int max(int... values){
        if(values.length == 0)
            return 0;
        int max = values[0];
        for( int number: values){
            if(number>max){
                max = number;
            }
        }
        return max;
    }

    public static int min(int... values){
        if(values.length == 0)
            return 0;
        int min = values[0];
        for( int number: values){
            if(number<min){
                min = number;
            }
        }
        return min;
    }
}
