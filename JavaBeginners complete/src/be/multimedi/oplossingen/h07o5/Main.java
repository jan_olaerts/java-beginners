package be.multimedi.oplossingen.h07o5;

public class Main {
    public static void main(String[] args){
        int[] numbers = { 4, 20, 10, 8, 13, 15 };
        System.out.format("Average: %d\n", Statistics.average(numbers));
        System.out.format("Min    : %d\n", Statistics.min(numbers));
        System.out.format("Max    : %d\n", Statistics.max(numbers));
    }
}
