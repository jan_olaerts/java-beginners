package be.multimedi.oplossingen.h07o3;

public class Main {
    public static void main(String[] args){
        int[][] numbers = new int[4][6];
        for(int i = 0; i<4; i++){
            for( int j = 0; j<6; j++){
                numbers[i][j] = (i+1)*(j+1);
            }
        }

        for(int iArray[]: numbers){
            for( int number: iArray) {
                System.out.print(number + " ");
            }
            System.out.println();
        }
    }
}
