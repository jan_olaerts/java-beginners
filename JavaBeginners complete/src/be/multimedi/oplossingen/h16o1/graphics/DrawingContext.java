package be.multimedi.oplossingen.h16o1.graphics;

/**
 * The Drawing context interface that should be able to handle the shapes
 */
public interface DrawingContext {
    public void draw(Rectangle rect);
    public void draw(Circle circle);
    public void draw(Triangle triangle);
}
