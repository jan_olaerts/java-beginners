package be.multimedi.oplossingen.h16o1.graphics;

import java.util.Formatter;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * The Drawing class as a list of Drawables is a Drawable itself
 */
public class Drawing implements Drawable, Iterable<Drawable> {
   /**
    * Constant for list maximum
    */
   static final int MAX_SIZE = 16;
   /**
    * size of the list currently stored
    */
   private int size;
   /**
    * storage space for Drawables
    */
   private Drawable[] list = new Drawable[MAX_SIZE];

   /**
    * A method te remove a Drawable on our drawing
    *
    * @param d the Drawable to remove
    */
   public void remove(Drawable d) {
      // look for d
      for (int i = 0; i < size; i++) {
         // is this the one?
         if (d == list[i]) {
            // move all the others 1 step forward
            for (int j = i + 1; j < size && j < list.length; j++) {
               list[j - 1] = list[j];
            }
            // remove the last (double)reference an decrease size
            list[size--] = null;
            return;
         }
      }
   }

   /**
    * A method to remove all Drawables and continue with a clean drawing
    */
   public void clear() {
      for (int i = 0; i < size; i++) {
         list[i] = null;
      }
      size = 0;
   }

   /**
    * A method to add Drawables to the drawing
    *
    * @param d The drawable to add to the drawing
    */
   public void add(Drawable d) {
      if (size >= MAX_SIZE) {
         return;
      }
      list[size] = d;
      size++;
   }

   /**
    * Gets the amount of drawables in the drawing
    *
    * @return The amount of drawables in the drawing
    */
   public int getSize() {
      return size;
   }

   /**
    * The method to draw everything to a display
    *
    * @param dc The DrawingContext responsible for display
    */
   @Override
   public void draw(DrawingContext dc) {
      //System.out.println("Drawing: ");
      for (Drawable d : list) {
         if (d != null) {
            d.draw(dc);
            //s.draw();
         }
      }
   }

   /**
    * Gets the String representing the square
    *
    * @return The String representing the square
    */
   @Override
   public String toString() {
      Formatter f = new Formatter();
      f.format("Drawing: %d/%d", size, MAX_SIZE);

      for (int i = 0; i < size; i++) {
         Drawable d = list[i];
         if (d == null) break;
         f.format("\n %d>%s", i + 1, d.toString());
      }
      return f.toString();
   }

   /**
    * Scales the rectangle up or down
    * scale 100 is 100% unchanged, 50 is 50% halved, 200 is 200% doubled
    *
    * @param factor The factor to scale with in %. Factor 200 means scaling 200%.
    */
   @Override
   public void scale(int factor) {
      for (int i = 0; i < size; i++) {
         list[i].scale(factor);
      }
   }

   /**
    * Gets the iterator for the list of drawables
    *
    * @return The iterator for the list of drawables
    */
   @Override
   public Iterator<Drawable> iterator() {
      return new DrawableIterator(this);
   }

   /**
    * The Nested class DrawableIterator that iterates our list of drawables
    */
   public class DrawableIterator implements Iterator<Drawable> {
      Drawing d;
      int index = 0;

      public DrawableIterator(Drawing d) {
         this.d = d;
      }

      @Override
      public boolean hasNext() {
         return index < d.size;
      }

      @Override
      public Drawable next() {
         if (index >= d.size) throw new NoSuchElementException();
         return d.list[index++];
      }

      @Override
      public void remove() {
         throw new UnsupportedOperationException();
      }
   }
}
