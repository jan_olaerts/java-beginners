package be.multimedi.oplossingen.h16o1.graphics;

import java.util.Formatter;

/**
 * This class is a square in a 2 dimensional area
 * This class can be used in the following way:
 * <pre>
 *    Square sq = new Square(5);
 * </pre>
 *
 * @author Ward Truyen
 * @version 1.0
 * @see be.multimedi.oplossingen.h16o1.graphics.Rectangle
 */
public class Square extends Rectangle {
   /**
    * number of instances of a Square.
    */
   private static int count = 0;

   /**
    * Creates an instance of the Square with 0 size
    */
   public Square() {
      this(0, 0, 0);
   }

   /**
    * Creates an instance of the Square
    *
    * @param side The initial size/side of the square
    * @throws NegativeSizeException when the size is negative
    */
   public Square(int side) {
      this(side, 0, 0);
   }

   /**
    * Creates an instance of the Square
    *
    * @param side The initial size/side of the square
    * @param x    The initial x position
    * @param y    The initial y position
    * @throws NegativeSizeException when the size is negative
    */
   public Square(int side, int x, int y) {
      count++;
      setSide(side);
      setPosition(x, y);
   }

   /**
    * Creates an instance of the Square
    *
    * @param s Square to copy
    * @throws NegativeSizeException when the size is negative
    */
   public Square(Square s) {
      this(s.getSide(), s.getX(), s.getY());
   }

   /**
    * Sets the size/side of the Square
    *
    * @param side The size/side of the square
    * @throws NegativeSizeException when the size is negative
    * @see be.multimedi.oplossingen.h16o1.graphics.Square#getSide
    */
   public void setSide(int side) {
      if (side < 0) throw new NegativeSizeException("Negative side");
      setWidth(side);
      setHeight(side);
   }

   /**
    * Gets the size/side of the Square
    *
    * @return The size/side of the square
    * @see be.multimedi.oplossingen.h16o1.graphics.Square#setSide
    */
   public int getSide() {
      return getWidth();
   }

   /**
    * Gets the number of squares made in the app
    *
    * @return The number of squares made in the app
    */
   public static int getCount() {
      return count;
   }

   /**
    * Gets the area of the square
    *
    * @return The area of the square
    */
   @Override
   public double getArea() {
      int w = getWidth();
      return w * w;
   }

   /**
    * Gets the perimeter of the square
    *
    * @return The perimeter of the square
    */
   @Override
   public double getPerimeter() {
      int w = getWidth();
      return w * 4;
   }

   /**
    * Make the square grow
    *
    * @param d Make the square grow by d units
    */
   @Override
   public void grow(int d) {
      setSide(getWidth() + d);
   }

   /**
    * Gets the String representing the square
    *
    * @return The String representing the square
    */
   @Override
   public String toString() {
      Formatter f = new Formatter().format("Square:");
      f.format("\n\tSide: %d", getSide());
      f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
      return f.toString();
   }

   /**
    * Gets the hash representing the square
    *
    * @return The hash representing the square
    */
   @Override
   public int hashCode() {
      //return super.hashCode();
      return getX() * 7 + getY() * 13 + getSide() * 17;
   }

   /**
    * Checks if 2 squares are the same
    *
    * @return True if the 2 squares are the same. False otherwise.
    */
   @Override
   public boolean equals(Object o) {
      //return super.equals(o);
      if (o != null && o.getClass() == getClass()) {
         Square s = (Square) o;
         if (s.getX() == getX() &&
                 s.getY() == getY() &&
                 s.getSide() == getSide()) {
            return true;
         }
      }
      return false;
   }
}
