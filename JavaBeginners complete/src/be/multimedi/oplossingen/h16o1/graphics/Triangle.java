package be.multimedi.oplossingen.h16o1.graphics;

import java.util.Formatter;

public class Triangle extends Shape {
    private static int count = 0;
    protected int width;
    protected int height;
    protected int perpendicular;

    public Triangle() {
        this(0, 0, 0, 0, 0);
    }

    public Triangle(int width, int height, int perpendicular) {
        this(width, height, perpendicular, 0, 0);
    }

    public Triangle(int width, int height, int perpendicular, int x, int y) {
        count++;
        setWidth(width);
        setHeight(height);
        setPerpendicular(perpendicular);
        setPosition(x, y);
    }

    public Triangle(Triangle t) {
        this(t.width, t.height, t.perpendicular, t.x, t.y);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width < 0)
            throw new NegativeSizeException("Negative width");
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height < 0)
            throw new NegativeSizeException("Negative height");
        this.height = height;
    }

    public int getPerpendicular() {
        return perpendicular;
    }

    public void setPerpendicular(int perpendicular) {
        this.perpendicular = perpendicular < 0 ? -perpendicular : perpendicular;
    }

    /**
     * Gets the area of the triangle
     *
     * @return The area of the triangle
     */
    @Override
    public double getArea() {
        return width * height / 2;
    }

    /**
     * Gets the perimeter of the triangle
     *
     * @return The perimeter of the triangle
     */
    @Override
    public double getPerimeter() {
        int temp = width - perpendicular;
        return Math.sqrt(perpendicular * perpendicular + height * height)
                + Math.sqrt(temp * temp + height * height);
    }

    /**
     * Make the triangle grow
     *
     * @param d Make the triangle grow by d units
     */
    @Override
    public void grow(int d) {
        setWidth(width + d);
        setHeight(height + d);
    }

    /**
     * Gets the String representing the triangle
     *
     * @return The String representing the triangle
     */
    @Override
    public String toString() {
        Formatter f = new Formatter().format("Triangle:");
        f.format("\n\tWidth: %d\n\tHeight: %d\n\tPerpendicular %d", width, height, perpendicular);
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    /**
     * Gets the hash representing the triangle
     *
     * @return The hash representing the triangle
     */
    @Override
    public int hashCode() {
        return getX() * 7 + getY() * 13 + getWidth() * 17 + getHeight() * 19 + getPerpendicular() * 23;
    }

    /**
     * Checks if 2 triangles are the same
     *
     * @return True if the 2 triangles are the same. False otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass() == getClass()) {
            Triangle t = (Triangle) o;
            if (t.getX() == getX() &&
                    t.getY() == getY() &&
                    t.getWidth() == getWidth() &&
                    t.getHeight() == getHeight() &&
                    t.getPerpendicular() == getPerpendicular()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Draws the triangle on screen by the use of the DrawingContext provided
     * @param dc The DrawingContext responsible for display
     */
    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    /**
     * Scales the triangle up or down
     * scale 100 is 100% unchanged, 50 is 50% halved, 200 is 200% doubled
     *
     * @param factor The factor to scale with in %. Factor 200 means scaling 200%.
     */
    @Override
    public void scale(int factor) {
        double f = (double)factor/100;
        setWidth((int)(width*f));
        setHeight((int)(height*f));
        setPerpendicular((int)(perpendicular*f));
    }

    /**
     * Gets the number of triangles made in the app
     *
     * @return The number of triangles made in the app
     */
    public static int getCount() {
        return count;
    }
}
