package be.multimedi.oplossingen.h16o1.graphics;

/**
 * The Scaleable interface for scalable objects
 */
public interface Scaleable {
   /**
    * The quarter size for scaling purposes
    */
   public static final int QUARTER = 25;

   /**
    * The half size for scaling purposes
    */
   public static final int HALF = 50;

   /**
    * The double size for scaling purposes
    */
   public static final int DOUBLE = 200;

   /**
    * Scales the scaleable up or down
    * scale 100 is 100% unchanged, 50 is 50% halved, 200 is 200% doubled
    *
    * @param factor The factor to scale with in %. Factor 200 means scaling 200%.
    */
   public abstract void scale(int factor);

   /**
    * Scales the scaleable up to double the size
    */
   public default void scaleDouble() {
      scale(DOUBLE);
   }

   /**
    * Scales the scaleable down to half the size
    */
   public default void scaleHalf() {
      scale(HALF);
   }
}
