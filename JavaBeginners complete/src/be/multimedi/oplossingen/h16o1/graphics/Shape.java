package be.multimedi.oplossingen.h16o1.graphics;

/**
 * The Shape class!! Who knows what shape it wil actually be?
 * It wont be a shape of the type Shape, because it's abstract, unfinished!
 * This class can be used in the following way:
 * <pre>
 *    Shape shape = new Rectangle();
 * </pre>
 *
 * @author Ward Truyen
 * @version 1.0
 * @see be.multimedi.oplossingen.h16o1.graphics.Drawable
 */
public abstract class Shape implements Drawable {
   /**
    * number of instances of a Shape.
    */
   private static int count = 0;

   /**
    * x-position of the shape.
    */
   protected int x;

   /**
    * x-position of the shape.
    */
   protected int y;

   /**
    * Creates an instance of the shape with 0 position
    */
   public Shape() {
      this(0, 0);
   }

   /**
    * Creates an instance of the shape with given position
    *
    * @param x      The initial x-position of the rectangle
    * @param y      The initial y-position of the rectangle
    */
   public Shape(int x, int y) {
      count++;
      setX(x);
      setY(y);
   }

   /**
    * Gets the x-position of the Shape
    *
    * @return The x-position of the Shape
    * @see be.multimedi.oplossingen.h16o1.graphics.Shape#setX
    */
   public int getX() {
      return x;
   }

   /**
    * Sets the x-position of the Shape
    *
    * @param x The x-position of the shape
    * @see be.multimedi.oplossingen.h16o1.graphics.Shape#getY
    */
   public void setX(int x) {
      this.x = x;
   }

   /**
    * Gets the y-position of the Shape
    *
    * @return The y-position of the Shape
    * @see be.multimedi.oplossingen.h16o1.graphics.Shape#setY
    */
   public int getY() {
      return y;
   }

   /**
    * Sets the y-position of the Shape
    *
    * @param y The y-position of the shape
    * @see be.multimedi.oplossingen.h16o1.graphics.Shape#getX
    */
   public void setY(int y) {
      this.y = y;
   }

   /**
    * Sets the x- and y-position of the Shape
    *
    * @param x The x-position of the shape
    * @param y The y-position of the shape
    * @see be.multimedi.oplossingen.h16o1.graphics.Shape#getX
    */
   public void setPosition(int x, int y) {
      setX(x);
      setY(y);
   }

   /**
    * Gets the area of the shape
    *
    * @return The area of the shape
    */
   public abstract double getArea();

   /**
    * Gets the perimeter of the shape
    *
    * @return The perimeter of the shape
    */
   public abstract double getPerimeter();

   /**
    * Make the shape grow
    *
    * @param d Make the shape grow by d units
    */
   public abstract void grow(int d);

   /**
    * Gets the number of shape made in the app
    *
    * @return The number of shape made in the app
    */
   public static int getCount() {
      return count;
   }
}
