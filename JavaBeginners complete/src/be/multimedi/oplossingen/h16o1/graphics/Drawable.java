package be.multimedi.oplossingen.h16o1.graphics;

/**
 * The interface drawable is an extension of a scalable
 */
public interface Drawable extends Scaleable {
   /**
    * The draw method using a DrawingContext
    *
    * @param dc The DrawingContext responsible for display
    */
   public void draw(DrawingContext dc);
}
