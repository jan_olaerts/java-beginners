package be.multimedi.oplossingen.h16o1.graphics;

import java.util.Formatter;

/**
 * This class is a circle in a 2 dimensional area
 * This class can be used in the following way:
 * <pre>
 *    Circle circle = new Circle(5);
 * </pre>
 *
 * @author Ward Truyen
 * @version 1.0
 * @see be.multimedi.oplossingen.h16o1.graphics.Shape
 */
public class Circle extends Shape {
    /**
     * number of instances of a Circle.
     */
    private static int count = 0;

    /**
     * radius of the Circle.
     */
    private int radius;

    /**
     * Creates an instance of the Circle with given size
     */
    public Circle() {
        this(0);
    }

    public Circle(int radius) {
        this(radius, 0, 0);
    }

    public Circle(int radius, int x, int y) {
        count++;
        setRadius(radius);
        setPosition(x, y);
    }

    public Circle(Circle c) {
        this(c.radius, c.x, c.y);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        if (radius < 0) throw new NegativeSizeException("Negative radius");
        this.radius = radius;
    }

    /**
     * Gets the area of the circle
     *
     * @return The area of the circle
     */
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    /**
     * Gets the perimeter of the circle
     *
     * @return The perimeter of the circle
     */
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    /**
     * Draws the circle on screen by the use of the DrawingContext provided
     * @param dc The DrawingContext responsible for display
     */
    @Override
    public void draw(DrawingContext dc) {
        dc.draw(this);
    }

    /**
     * Make the circle grow
     *
     * @param d Make the circle grow by d units
     */
    @Override
    public void grow(int d) {
        setRadius(radius + d);
    }

    /**
     * Gets the String representing the circle
     *
     * @return The String representing the circle
     */
    @Override
    public String toString() {
        Formatter f = new Formatter().format("Circle:");
        f.format("\n\tRadius: %d", radius);
        f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
        return f.toString();
    }

    /**
     * Gets the hash representing the circle
     *
     * @return The hash representing the circle
     */
    @Override
    public int hashCode() {
        return getX() * 7 + getY() * 13 + getRadius() * 17;
    }

    /**
     * Checks if 2 circles are the same
     *
     * @return True if the 2 circles are the same. False otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (getClass() == o.getClass()) {
                Circle c = (Circle) o;
                if ((c.getX() == getX()) &&
                        (c.getY() == getY()) &&
                        c.getRadius() == getRadius()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Scales the circle up or down
     * scale 100 is 100% unchanged, 50 is 50% halved, 200 is 200% doubled 
     *
     * @param factor The factor to scale with in %. Factor 200 means scaling 200%.
     */
    @Override
    public void scale(int factor) {
        setRadius((int) ((double) factor / 100 * radius));
    }

    /**
     * Gets the number of circles made in the app
     *
     * @return The number of circles made in the app
     */
    public static int getCount() {
        return count;
    }
}
