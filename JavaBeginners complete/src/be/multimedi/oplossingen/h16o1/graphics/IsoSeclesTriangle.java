package be.multimedi.oplossingen.h16o1.graphics;

/**
 * Nederlands: Gelijkbenige driehoek /\
 */
public class IsoSeclesTriangle extends Triangle {
    private static int count = 0;

    IsoSeclesTriangle(int x, int y, int w, int h){
        this.setX(x);
        this.setY(y);
        this.setWidth(w);
        this.setHeight(h);
    }

    IsoSeclesTriangle(){
        this(0,0,0,0);
    }

    IsoSeclesTriangle(int w, int h){
        this(0,0,w,h);
    }

    IsoSeclesTriangle(IsoSeclesTriangle copy){
        this(copy.x, copy.y, copy.width, copy.height);
    }

    /**
     * Gets the number of IsoSeclesTriangles made in the app
     *
     * @return The number of IsoSeclesTriangles made in the app
     */
    public static int getCount() {
        return count;
    }

    @Override
    public void setWidth(int width){
        if(width < 0) width = -width;
        this.width = width;
        this.perpendicular = width /2;
    }

    @Override
    public void setPerpendicular(int perpendicular){
        if( perpendicular < 0 ) perpendicular = -perpendicular;
        this.perpendicular = perpendicular;
        this.width = perpendicular * 2;
    }

    /**
     * Gets the String representing the IsoSeclesTriangle
     *
     * @return The String representing the IsoSeclesTriangle
     */
    @Override
    public String toString() {
        return "IsoSeclesTriangle{" +
                "width=" + width +
                ", height=" + height +
                ", perpendicular=" + perpendicular +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    /**
     * Gets the hash representing the IsoSeclesTriangle
     *
     * @return The hash representing the IsoSeclesTriangle
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Checks if 2 IsoSeclesTriangles are the same
     *
     * @return True if the 2 IsoSeclesTriangles are the same. False otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
