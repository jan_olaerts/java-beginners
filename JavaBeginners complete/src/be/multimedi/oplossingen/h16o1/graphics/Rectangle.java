package be.multimedi.oplossingen.h16o1.graphics;

import java.util.Formatter;

/**
 * This class is a rectangle in a 2 dimensional area
 * This class can be used in the following way:
 * <pre>
 *    Rectangle rectangle = new Rectangle(5);
 * </pre>
 *
 * @author Ward Truyen
 * @version 1.0
 * @see be.multimedi.oplossingen.h16o1.graphics.Shape
 */
public class Rectangle extends Shape {
   /**
    * number of instances of a Rectangle.
    */
   private static int count = 0;

   /**
    * width of the rectangle.
    */
   protected int width;

   /**
    * height of the rectangle.
    */
   protected int height;

   /**
    * Creates an instance of the Rectangle with given size
    */
   public Rectangle() {
      this(0, 0, 0, 0);
   }

   /**
    * Creates an instance of the Rectangle with 0 size
    *
    * @param width  The initial width of the rectangle
    * @param height The initial height of the rectangle
    */
   public Rectangle(int width, int height) {
      this(width, height, 0, 0);
   }

   /**
    * Creates an instance of the Rectangle with given size and position
    *
    * @param width  The initial width of the rectangle
    * @param height The initial height of the rectangle
    * @param x      The initial x-position of the rectangle
    * @param y      The initial y-position of the rectangle
    */
   public Rectangle(int width, int height, int x, int y) {
      count++;
      setWidth(width);
      setHeight(height);
      setX(x);
      setY(y);
   }

   /**
    * Creates an instance of the Rectangle by copy
    *
    * @param r the object to make a copy from
    */
   public Rectangle(Rectangle r) {
      this(r.width, r.height, r.x, r.y);
   }

   /**
    * Gets the width of the Rectangle
    *
    * @return The width of the rectangle
    * @see be.multimedi.oplossingen.h16o1.graphics.Rectangle#setWidth
    */
   public int getWidth() {
      return width;
   }

   /**
    * Sets the size/side of the Rectangle
    *
    * @param width The size/side of the rectangle
    * @throws NegativeSizeException when the width is negative
    * @see be.multimedi.oplossingen.h16o1.graphics.Rectangle#getWidth
    */
   public void setWidth(int width) {
      if (width < 0)
         throw new NegativeSizeException("Negative width");
      this.width = width;
   }

   /**
    * Gets the height of the Rectangle
    *
    * @return The height of the rectangle
    * @see be.multimedi.oplossingen.h16o1.graphics.Rectangle#setHeight
    */
   public int getHeight() {
      return height;
   }

   /**
    * Sets the size/side of the Rectangle
    *
    * @param height The height of the rectangle
    * @throws NegativeSizeException when the height is negative
    * @see be.multimedi.oplossingen.h16o1.graphics.Rectangle#getHeight()
    */
   public void setHeight(int height) {
      if (height < 0)
         throw new NegativeSizeException("Negative height");
      this.height = height;
   }

   /**
    * Gets the area of the rectangle
    *
    * @return The area of the rectangle
    */
   @Override
   public double getArea() {
      return width * height;
   }

   /**
    * Gets the perimeter of the rectangle
    *
    * @return The perimeter of the rectangle
    */
   @Override
   public double getPerimeter() {
      return (width + height) * 2;
   }

   /**
    * Make the rectangle grow
    *
    * @param d Make the rectangle grow by d units
    */
   @Override
   public void grow(int d) {
      setWidth(width + d);
      setHeight(height + d);
   }

   /**
    * Gets the String representing the rectangle
    *
    * @return The String representing the rectangle
    */
   @Override
   public String toString() {
      Formatter f = new Formatter().format("Rectangle:");
      f.format("\n\tWidth: %d\n\tHeight: %d", width, height);
      f.format("\n\tPosX: %d\n\tPosY: %d", x, y);
      return f.toString();
   }

   /**
    * Gets the hash representing the rectangle
    *
    * @return The hash representing the rectangle
    */
   @Override
   public int hashCode() {
      return getX() * 7 + getY() * 13 + height * 17 + width * 19;
   }

   /**
    * Checks if 2 rectangles are the same
    *
    * @return True if the 2 rectangles are the same. False otherwise.
    */
   @Override
   public boolean equals(Object o) {
      if (o != null && getClass() == o.getClass()) {
         Rectangle r = (Rectangle) o;
         if ((r.getX() == getX()) &&
                 (r.getY() == getY()) &&
                 (r.width == width) &&
                 (r.height == height)) {
            return true;
         }
      }
      return false;
   }

   /**
    * Draws the rectangle on screen by the use of the DrawingContext provided
    *
    * @param dc The DrawingContext responsible for display
    */
   @Override
   public void draw(DrawingContext dc) {
      dc.draw(this);
   }

   /**
    * Scales the rectangle up or down
    * scale 100 is 100% unchanged, 50 is 50% halved, 200 is 200% doubled
    *
    * @param factor The factor to scale with in %. Factor 200 means scaling 200%.
    */
   @Override
   public void scale(int factor) {
      double f = (double) factor / 100;
      setWidth((int) (width * f));
      setHeight((int) (height * f));
   }

   /**
    * Gets the number of rectangle made in the app
    *
    * @return The number of rectangle made in the app
    */
   public static int getCount() {
      return count;
   }
}
