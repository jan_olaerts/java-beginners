package be.multimedi.oplossingen.h16o1.consoleTools;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A Utility class to get input from the user using the console
 */
public final class ConsoleInputTool {
   /**
    * Constructor
    */
   private ConsoleInputTool() {
   }

   /**
    * keyboard/Scanner
    */
   static Scanner keyboard = new Scanner(System.in);

   /**
    * Requests the user to press the return key to continue.
    */
   public static void askPressEnterToContinue() {
      System.out.print("Press enter to continue.");
      keyboard.nextLine();
   }

   /**
    * Ask the user for a boolean(may repeat untill input is correct).
    *
    * @param question        The question to ask(print to) the user.
    * @param useDefaultValue Use the default value if the user inputs a blank line? yes or no ? true or false.
    * @param defaultValue    The default value to use if the user inputs a blank line.
    * @return the user input: string.
    */
   public static boolean askUserYesNoQuestion(String question, boolean useDefaultValue, boolean defaultValue) {
      do {
         System.out.print(question);
         String answer = keyboard.nextLine();
         answer = answer.toLowerCase();
         if (answer.equals("y") || answer.equals("yes")) return true;
         else if (answer.equals("n") || answer.equals("no")) return false;
         else if (useDefaultValue && answer.isBlank()) return defaultValue;
         System.err.println("Error: input must be y or n.");
      } while (true);
   }

   /**
    * Ask the user for a boolean(repeat untill input is correct).
    *
    * @param question The question to ask(print to) the user.
    * @return the user input: string.
    */
   public static boolean askUserYesNoQuestion(String question) {
      return askUserYesNoQuestion(question, false, false);
   }

   /**
    * Ask the user for a integer(repeat untill input is correct).
    *
    * @param question the question to ask(print to) the user.
    * @return the user input: integer.
    */
   public static int askUserInteger(String question) {
      int input = 0;
      while (true) {
         try {
            System.out.print(question);
            input = keyboard.nextInt();
            break;
         } catch (InputMismatchException ime) {
            System.err.println("Error: input is not a number");
         } finally {
            keyboard.nextLine();
         }
      }
      return input;
   }

   /**
    * Ask the user for a integer(repeat untill input is correct).
    *
    * @param question the question to ask(print to) the user.
    * @param minimum  the minimum the integer is allowed to be.
    * @param maximum  the maximum the integer is allowed to be.
    * @return the user input: integer.
    */
   public static int askUserInteger(String question, int minimum, int maximum) {
      int input = 0;
      do {
         input = askUserInteger(question);
         if (input < minimum) {
            System.err.println("Error: input must be equal or higher than " + minimum);
         } else if (input > maximum) {
            System.err.println("Error: input must be equal or lower than " + maximum);
         }
      } while (input < minimum || input > maximum);
      return input;
   }
}
