package be.multimedi.oplossingen.h16o1.consoleTools;

public final class ConsoleOutputTool {
   /**
    * Constructor
    */
   private ConsoleOutputTool() {
   }

   /**
    * default title underlining character
    */
   static final char DEFAULT_TITLE_LINE_CHAR = '_';

   /**
    * Decorate String as a title and print it at the same time
    *
    * @param text message to decorate
    * @param type the character underlining the title
    */
   public static void printTitle(String text, char type) {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < text.length(); i++) {
         sb.append(type);
      }
      System.out.println(text);
      System.out.println(sb.toString());
   }

   /**
    * Decorate String as a title and print it at the same time
    *
    * @param text message to decorate
    */
   public static void printTitle(String text) {
      printTitle(text, DEFAULT_TITLE_LINE_CHAR);
   }

}
