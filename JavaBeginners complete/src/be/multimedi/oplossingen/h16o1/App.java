package be.multimedi.oplossingen.h16o1;

import be.multimedi.oplossingen.h16o1.graphics.*;
import be.multimedi.oplossingen.h16o1.consoleTools.*;

import java.util.Formatter;

/**
 * This class is the starting point of the app
 * This class can be used in the following way:
 * <pre>
 *    App app = new App().start();
 * </pre>
 *
 * @author Ward Truyen
 * @version 1.0
 * @see be.multimedi.oplossingen.h16o1.graphics.Drawable
 */
public class App {
   Drawing drawing = new Drawing();

   /**
    * This is the starting method of the app-blueprint
    *
    * To generate the javaDoc use:
    *    javadoc -author -version -d docs -sourcepath src be.multimedi.oplossingen.h16o1
    *    javadoc -author -version -d docs -sourcepath src be.multimedi.oplossingen.h16o1.graphics
    *    javadoc -author -version -d docs -sourcepath src be.multimedi.oplossingen.h16o1.consoleTools
    *
    * @param args Arguments of the application (not used)
    */
   public static void main(String[] args) {
      App app = new App();
      if (ConsoleInputTool.askUserYesNoQuestion("Add test data? (y/n empty=y): ", true, true))
         app.addTestData();
      app.start();
   }

   /**
    * This method will add some test data to the drawing
    */
   public void addTestData() {
      System.out.println("Adding test data: 1 Rectangle, 1 Square, 1 Triangle and 1 Circle");
      Rectangle rectangle = new Rectangle(10, 10, 1, 2);
      Square square = new Square(40, 11, 12);
      Triangle triangle = new Triangle(10, 20, 30, 21, 22);
      Circle circle = new Circle(20, 31, 32);

      drawing.add(rectangle);
      drawing.add(square);
      drawing.add(triangle);
      drawing.add(circle);
   }

   /**
    * This is the starting method of the app-instance
    */
   public void start() {
      int choice;
      do {
         System.out.println();
         ConsoleOutputTool.printTitle("Drawing App");
         System.out.println("1. Overview drawing");
         System.out.println("2. Add to drawing");
         System.out.println("3. Scale drawing");
         System.out.println("4. Clear drawing");
         System.out.println("5. Overview Shape counters");
         System.out.println("0. Exit app");
         choice = ConsoleInputTool.askUserInteger("Your choice: ", 0, 5);
         switch (choice) {
            case 1:
               printDrawing();
               break;
            case 2:
               addToDrawingMenu();
               break;
            case 3:
               System.out.println("Scale is in %, so 200 becomes 200%. No floating point.");
               drawing.scale(ConsoleInputTool.askUserInteger("Scale: "));
               printDrawing();
               break;
            case 4:
               if (ConsoleInputTool.askUserYesNoQuestion("Are you sure you want to clear the drawing?(y/n): ")) {
                  System.out.println("Clearing drawing");
                  drawing.clear();
                  printDrawing();
               }
               break;
            case 5:
               printShapeCount();
               break;
         }
      } while (choice != 0);
   }

   /**
    * print the contents of the drawing on the terminal
    */
   void printDrawing() {
      System.out.println();
      ConsoleOutputTool.printTitle("Drawing:");
      System.out.println(" Size: " + drawing.getSize());
      drawing.draw(new TextDrawingContext());
      ConsoleInputTool.askPressEnterToContinue();
   }

   /**
    * print the contents of the Shape counters on the terminal
    */
   void printShapeCount() {
      Formatter f = new Formatter();
      f.format("\n");
      f.format("Created Shapes:      %d\n", Shape.getCount());
      f.format(" Rectangles:         %d\n", Rectangle.getCount());
      f.format(" Squares:            %d\n", Square.getCount());
      f.format(" Circles:            %d\n", Circle.getCount());
      f.format(" Triangles:          %d\n", Triangle.getCount());
      f.format(" IsoSeclesTriangles: %d", IsoSeclesTriangle.getCount());
      System.out.println(f.toString());
   }

   /**
    * Add to drawing Menu
    */
   void addToDrawingMenu() {
      while (true) {
         System.out.println();
         ConsoleOutputTool.printTitle("Add to drawing menu");
         System.out.println("1. Add Rectangle");
         System.out.println("2. Add Square");
         System.out.println("3. Add Circle");
         System.out.println("4. Add Triangle");
         System.out.println("5. Add IsoSeclesTriangle");
         System.out.println("0. Back to main menu");
         int choice = ConsoleInputTool.askUserInteger("Your choice: ", 0, 5);
         switch (choice) {
            case 1:
               addRectangle();
               break;
            case 2:
               addSquare();
               break;
            case 3:
               addCircle();
               break;
            case 4:
               //TODO addTriangle
//                   addTriangle();
//                   break;
            case 5:
               //TODO addIsoSeclesTriangle
               System.out.println("Not programmed yet!");
            default:
               System.err.println("Unknown option.");
               try {
                  Thread.sleep(100);
               }catch (Exception e){}
            case 0:
               return;
         }
      }
   }

   /**
    * Add Rectangle to drawing process
    */
   void addRectangle() {
      ConsoleOutputTool.printTitle("Adding a rectangle to drawing");
      int x = ConsoleInputTool.askUserInteger("Position x: ");
      int y = ConsoleInputTool.askUserInteger("Position y: ");
      int width = ConsoleInputTool.askUserInteger("Width: ");
      int height = ConsoleInputTool.askUserInteger("Height: ");
      try {
         Rectangle rectangle = new Rectangle(width, height, x, y);
         drawing.add(rectangle);
      } catch (Exception e) {
         System.err.println("Error: Failed to add rectangle: " + e.getMessage());
         //e.printStackTrace();
      }
   }

   /**
    * Add Square to drawing process
    */
   void addSquare() {
      ConsoleOutputTool.printTitle("Adding a square to drawing");
      int x = ConsoleInputTool.askUserInteger("Position x: ");
      int y = ConsoleInputTool.askUserInteger("Position y: ");
      int size = ConsoleInputTool.askUserInteger("Size: ");
      try {
         drawing.add(new Square(size, x, y));
      } catch (Exception e) {
         System.err.println("Error: Failed to add square: " + e.getMessage());
         //e.printStackTrace();
      }
   }

   /**
    * Add Circle to drawing process
    */
   void addCircle() {
      ConsoleOutputTool.printTitle("Adding a circle to drawing");
      int x = ConsoleInputTool.askUserInteger("Position x: ");
      int y = ConsoleInputTool.askUserInteger("Position y: ");
      int size = ConsoleInputTool.askUserInteger("Radius: ");
      try {
         drawing.add(new Circle(size, x, y));
      } catch (Exception e) {
         System.err.println("Error: Failed to add circle: " + e.getMessage());
         //e.printStackTrace();
      }
   }
}
