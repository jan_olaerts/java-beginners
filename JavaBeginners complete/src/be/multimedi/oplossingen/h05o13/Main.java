package be.multimedi.oplossingen.h05o13;

public class Main {
    public static void main(String[] args){
        /*
        {
            int number 123;// Error: Java: cannot find symbol: variable number @ System.out.println().
        }
        // */
        int number = 123;
        {
            //int number = 321;// Error: Java: variable number is already defined in method main @ this line
            System.out.println("number\t: " + number);
        }
        //System.out.println("number\t: " + number); // OK
    }
}
