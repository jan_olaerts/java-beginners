package be.multimedi.oplossingen.h06o1;

//# import van java.util
import java.util.*;
//import java.util.Random; //# deze import is selectiever. En volstaat voor in deze oefening.

public class RandomApp {
    public static void main(String[] args) {
        Random rnd = new Random();
        System.out.println("*** 3x willekeurige getallen ***");
        for (int i = 0; i < 3; i++) {
            System.out.println(rnd.nextInt());
        }

        System.out.println("*** 3x willekeurige getallen tussen 100 en 200 ***");
        for (int i = 0; i < 3; i++) {
            System.out.println(rnd.nextInt(100) + 100);
        }

        System.out.println("*** (lotto) 6x willekeurige getallen tussen 0 en 45 ***");
        for (int i = 0; i < 6; i++) {
            System.out.println(rnd.nextInt(45));
        }
    }
}
