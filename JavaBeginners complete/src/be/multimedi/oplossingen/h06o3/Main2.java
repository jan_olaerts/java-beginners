package be.multimedi.oplossingen.h06o3;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String text = keyboard.nextLine();

        printStringValue("Lenght", text.length());
        printStringValue("Upper case", text.toUpperCase());
        printStringValue("Lower case", text.toLowerCase());
        printStringValue("'a'->'o'", text.replace('a', 'o'));

        String text2 = "Hello";
        String text3 = "World";
        printStringValue("text2", text2);
        printStringValue("text3", text3);
        printStringValue("text2==text3", text2.equals(text3));
        printStringValue("Alphabetical first", text2.compareTo(text3) > 0  ? text3 : text2);

        text = "     Hello Spaces     ";
        printStringValue("Untrimmed", text);
        printStringValue("Trimmed", text.trim());
    }

    public static void printStringValue(String descr, String value){
        System.out.format("%18s : \"%s\"\n", descr, value);
    }
    public static void printStringValue(String descr, Object value){
        System.out.format("%18s : %s\n", descr, value.toString());
    }
}
