package be.multimedi.oplossingen.h06o3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String text = keyboard.nextLine();

        System.out.println("Lenght\t\t\t: " + text.length());
        System.out.println("Upper case\t\t: " + text.toUpperCase());
        System.out.println("Lower case\t\t: " + text.toLowerCase());
        text.replace('a', 'o');
        System.out.println("'a'->'o'\t\t: " + text);

        String text2 = "Hello";
        String text3 = "World";
        System.out.println("text2\t\t\t: " + text2);
        System.out.println("text3\t\t\t: " + text3);
        System.out.println("text2==text3\t: " + text2.equals(text3));
        String text4 = text2.compareTo(text3) > 0  ? text3 : text2;
        System.out.println("Alphabetical first\t: " + text4);

        text = "   Hello Spaces    ";
        System.out.println("text\t\t\t: \"" + text + "\"");
        text  = text.trim();
        System.out.println("trimmed text\t: \"" + text + "\"");
    }
}
